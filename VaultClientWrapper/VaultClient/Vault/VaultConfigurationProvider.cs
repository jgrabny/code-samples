﻿namespace VaultClient.Vault
{
    using System.Configuration;

    public class VaultConfigurationProvider : IVaultConfigurationProvider
    {
        private const string ConnectionStringKey = "vaultConnectionString";
        private const string AuthenticationModeKey = "vaultAuthenticationMode";
        private const string AuthenticationTokenKey = "vaultAuthenticationToken";
        private const string AuthenticationCertificateThumbprint = "vaultAuthenticationCertificateThumbprint";

        public string GetTokenKey()
        {
            return ConfigurationManager.AppSettings[AuthenticationTokenKey];
        }

        public string GetCertificateThumbprint()
        {
            return ConfigurationManager.AppSettings[AuthenticationCertificateThumbprint];
        }

        public string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings[ConnectionStringKey]?.ConnectionString;
        }

        public string GetAuthenticationMode()
        {
            return ConfigurationManager.AppSettings[AuthenticationModeKey];
        }
    }
}