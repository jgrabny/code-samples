﻿namespace VaultClient.Vault
{
    using VaultSharp;

    public class VaultClientProvider : IVaultClientProvider
    {
        private readonly IVaultUriProvider vaultUriProvider;
        private readonly IAuthenticationInfoFactory authenticationInfoFactory;

        public VaultClientProvider(
            IVaultUriProvider vaultUriProvider,
            IAuthenticationInfoFactory authenticationInfoFactory)
        {
            this.vaultUriProvider = vaultUriProvider;
            this.authenticationInfoFactory = authenticationInfoFactory;
        }

        public IVaultClient GetVaultClient()
        {
            var authenticationInfo = authenticationInfoFactory.GetAuthenticationInfo();
            var vaultUri = vaultUriProvider.GetVaultUri();

            return VaultClientFactory.CreateVaultClient(vaultUri, authenticationInfo);
        }
    }
}