﻿namespace VaultClient.Vault
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IVaultClientWrapper
    {
        Task Write(string path, string key, byte[] value);

        Task<object> Read(string path, string key);

        Task<byte[]> ReadBytes(string path, string key);

        Task<IEnumerable<string>> ListKeys(string path = "");
    }
}