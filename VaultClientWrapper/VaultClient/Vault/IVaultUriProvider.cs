﻿namespace VaultClient.Vault
{
    using System;

    public interface IVaultUriProvider
    {
        Uri GetVaultUri();
    }
}