﻿namespace VaultClient.Vault
{
    using System;

    public class VaultUriProvider : IVaultUriProvider
    {
        private readonly IVaultConfigurationProvider vaultConfigurationProvider;

        public VaultUriProvider(IVaultConfigurationProvider vaultConfigurationProvider)
        {
            this.vaultConfigurationProvider = vaultConfigurationProvider;
        }

        public Uri GetVaultUri()
        {
            var vaultConnectionString = vaultConfigurationProvider.GetConnectionString();

            return new Uri(vaultConnectionString);
        }
    }
}