﻿namespace VaultClient.Vault
{
    using System;

    using EncryptionCommon.Certificate;

    using VaultSharp.Backends.Authentication.Models;
    using VaultSharp.Backends.Authentication.Models.Certificate;
    using VaultSharp.Backends.Authentication.Models.Token;

    public class AuthenticationInfoFactory : IAuthenticationInfoFactory
    {
        private readonly IVaultConfigurationProvider vaultConfigurationProvider;
        private readonly ICertificateProvider certificateProvider;

        public AuthenticationInfoFactory(
            IVaultConfigurationProvider vaultConfigurationProvider,
            ICertificateProvider certificateProvider)
        {
            this.vaultConfigurationProvider = vaultConfigurationProvider;
            this.certificateProvider = certificateProvider;
        }

        public IAuthenticationInfo GetAuthenticationInfo()
        {
            var authenticationMode = vaultConfigurationProvider.GetAuthenticationMode();

            switch (authenticationMode)
            {
                case "token":
                {
                    return CreateTokenAuthenticationInfo();
                }

                case "certificate":
                {
                    return CreateCertificateAuthenticationInfo();
                }

                default:
                {
                    throw new ArgumentException($"`{authenticationMode}` authentication mode is not supported.");
                }
            }
        }

        private IAuthenticationInfo CreateCertificateAuthenticationInfo()
        {
            var certificateThumbprint = vaultConfigurationProvider.GetCertificateThumbprint();
            var certificate = certificateProvider.GetCertificate(certificateThumbprint);

            return new CertificateAuthenticationInfo(certificate);
        }

        private IAuthenticationInfo CreateTokenAuthenticationInfo()
        {
            var token = vaultConfigurationProvider.GetTokenKey();

            if (string.IsNullOrEmpty(token))
            {
                throw new ArgumentException($"`Token` value is missing from the appSettings.");
            }

            return new TokenAuthenticationInfo(token);
        }
    }
}