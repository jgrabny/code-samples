﻿namespace VaultClient.Vault
{
    public interface IVaultConfigurationProvider
    {
        string GetTokenKey();

        string GetCertificateThumbprint();

        string GetConnectionString();
        
        string GetAuthenticationMode();
    }
}