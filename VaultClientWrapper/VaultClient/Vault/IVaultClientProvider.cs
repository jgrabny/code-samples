﻿namespace VaultClient.Vault
{
    using VaultSharp;

    public interface IVaultClientProvider
    {
        IVaultClient GetVaultClient();
    }
}