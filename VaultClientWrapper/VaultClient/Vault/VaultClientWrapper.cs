﻿namespace VaultClient.Vault
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using VaultSharp;

    public class VaultClientWrapper : IVaultClientWrapper
    {
        private readonly IVaultClientProvider vaultClientProvider;
        private IVaultClient client;

        public VaultClientWrapper(
            IVaultClientProvider vaultClientProvider)
        {
            this.vaultClientProvider = vaultClientProvider;
        }

        private IVaultClient Client => client ?? (client = vaultClientProvider.GetVaultClient());

        public async Task Write(string path, string key, byte[] value)
        {
            var values = new Dictionary<string, object> { { key, value } };

            await Client.WriteSecretAsync(path, values);
        }

        public async Task<byte[]> ReadBytes(string path, string key)
        {
            var result = await Read(path, key);
            return Convert.FromBase64String(result.ToString());
        }

        public async Task<object> Read(string path, string key)
        {
            return (await Client.ReadSecretAsync(path)).Data[key];
        }

        public async Task<IEnumerable<string>> ListKeys(string path = "")
        {
            var result = await Client.GenericReadSecretLocationPathListAsync(path);
            return result.Data.Keys;
        }
    }
}