﻿namespace VaultClient.Vault
{
    using VaultSharp.Backends.Authentication.Models;

    public interface IAuthenticationInfoFactory
    {
        IAuthenticationInfo GetAuthenticationInfo();
    }
}