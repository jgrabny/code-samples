﻿namespace VaultClient.IoC
{
    using Autofac;

    using VaultClient.Vault;

    public class VaultClientModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AuthenticationInfoFactory>().As<IAuthenticationInfoFactory>();
            builder.RegisterType<VaultClientWrapper>().As<IVaultClientWrapper>();
            builder.RegisterType<VaultClientProvider>().As<IVaultClientProvider>();
            builder.RegisterType<VaultUriProvider>().As<IVaultUriProvider>();
            builder.RegisterType<VaultConfigurationProvider>().As<IVaultConfigurationProvider>();
        }
    }
}