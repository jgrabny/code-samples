﻿namespace VaultClient.Tests.VaultTests
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;

    using Moq;

    using NUnit.Framework;

    using VaultClient.Vault;

    using VaultSharp;
    using VaultSharp.Backends.Secret.Models;
    using VaultSharp.Backends.System.Models;

    [TestFixture]
    public class VaultClientWrapperTests
    {
        private Mock<IVaultClientProvider> vaultClientProviderMock;
        private Mock<IVaultClient> vaultClientMock;

        private IVaultClientWrapper vaultClientWrapper;

        [SetUp]
        public void SetUp()
        {
            vaultClientProviderMock = new Mock<IVaultClientProvider>();
            vaultClientMock = new Mock<IVaultClient>();

            vaultClientProviderMock.Setup(x => x.GetVaultClient()).Returns(vaultClientMock.Object);

            vaultClientWrapper = new VaultClientWrapper(vaultClientProviderMock.Object);
        }

        [Test]
        public async Task ShouldWriteToVaultWithPath()
        {
            // given 
            var expectedPath = "secret";
            var expectedKey = "key";
            var expectedValue = Encoding.UTF8.GetBytes("somevalue");

            // when
            await vaultClientWrapper.Write(expectedPath, expectedKey, expectedValue);

            // then
            vaultClientMock.Verify(
                x => x.WriteSecretAsync(
                expectedPath, 
                It.Is<IDictionary<string, object>>(y => y[expectedKey] == expectedValue)),
                Times.Once);
        }

        [Test]
        public async Task ShouldReadFromVaultWithPath()
        {
            // given
            var expectedPath = "secret";
            var expectedKey = "key";
            var expectedValue = Encoding.UTF8.GetBytes("somevalue");
            var vaultResult = new Secret<Dictionary<string, object>>
            {
                Data = new Dictionary<string, object> { { expectedKey, expectedValue } }
            };

            vaultClientMock
                .Setup(x => x.ReadSecretAsync(expectedPath))
                .ReturnsAsync(vaultResult);

            // when
            var result = await vaultClientWrapper.Read(expectedPath, expectedKey);

            // then
            Assert.That(result, Is.EqualTo(expectedValue));
        }

        [Test]
        public async Task ShouldReadBytesFromVaultWithPath()
        {
            // given
            var expectedPath = "secret";
            var expectedKey = "key";
            var expectedValue = "somevalue";
            var expectedValueBytes = Encoding.UTF8.GetBytes(expectedValue);
            var expectedValueStoredInVault = Convert.ToBase64String(expectedValueBytes);

            var vaultResult = new Secret<Dictionary<string, object>>
            {
                Data = new Dictionary<string, object> { { expectedKey, expectedValueStoredInVault } }
            };

            vaultClientMock
                .Setup(x => x.ReadSecretAsync(expectedPath))
                .ReturnsAsync(vaultResult);

            // when
            var result = await vaultClientWrapper.ReadBytes(expectedPath, expectedKey);

            // then
            Assert.That(result, Is.EqualTo(expectedValueBytes));
        }

        [Test]
        public async Task ShouldListFromPath()
        {
            // given
            var expectedPath = "secret";

            var vaultResult = new Secret<ListInfo>
            {
                Data = new ListInfo
                {
                    Keys = new List<string>()
                }
            };

            vaultClientMock
                .Setup(x => x.GenericReadSecretLocationPathListAsync(expectedPath, "secret", null))
                .ReturnsAsync(vaultResult);

            // when
            var result = await vaultClientWrapper.ListKeys(expectedPath);

            // then
            Assert.That(result, Is.EqualTo(vaultResult.Data.Keys));
        }
    }
}