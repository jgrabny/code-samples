﻿namespace VaultClient.Tests.VaultTests
{
    using System;

    using Moq;

    using NUnit.Framework;

    using VaultClient.Vault;

    [TestFixture]
    public class VaultUriProviderTests
    {
        private Mock<IVaultConfigurationProvider> vaultConfigurationProviderMock;

        private IVaultUriProvider provider;

        [SetUp]
        public void SetUp()
        {
            vaultConfigurationProviderMock = new Mock<IVaultConfigurationProvider>();

            provider = new VaultUriProvider(vaultConfigurationProviderMock.Object);
        }

        [Test]
        public void ShouldGetVaultUri()
        {
            // given
            var expectedConnectionString = "http://localtest:1234";

            vaultConfigurationProviderMock
                .Setup(x => x.GetConnectionString())
                .Returns(expectedConnectionString);

            // when
            var result = provider.GetVaultUri();

            // then
            Assert.That(result, Is.EqualTo(new Uri(expectedConnectionString)));
        }        
    }
}