﻿namespace VaultClient.Tests.VaultTests
{
    using System;
    using System.Security.Cryptography.X509Certificates;

    using EncryptionCommon.Certificate;

    using Moq;

    using NUnit.Framework;

    using VaultClient.Vault;

    using VaultSharp.Backends.Authentication.Models;
    using VaultSharp.Backends.Authentication.Models.Certificate;
    using VaultSharp.Backends.Authentication.Models.Token;

    [TestFixture]
    public class AuthenticationInfoFactoryTests
    {
        private Mock<IVaultConfigurationProvider> vaultConfigurationProviderMock;
        private Mock<ICertificateProvider> certificateProviderMock;

        private IAuthenticationInfoFactory factory;

        [SetUp]
        public void SetUp()
        {
            vaultConfigurationProviderMock = new Mock<IVaultConfigurationProvider>();
            certificateProviderMock = new Mock<ICertificateProvider>();

            factory = new AuthenticationInfoFactory(
                vaultConfigurationProviderMock.Object,
                certificateProviderMock.Object);
        }

        [Test]
        public void ShouldGetTokenAuthenticationInfo()
        {
            // given
            var expectedToken = "123";

            vaultConfigurationProviderMock.Setup(x => x.GetAuthenticationMode()).Returns("token");
            vaultConfigurationProviderMock.Setup(x => x.GetTokenKey()).Returns(expectedToken);

            // when
            var authenticationInfo = factory.GetAuthenticationInfo();

            // then
            Assert.That(authenticationInfo.AuthenticationBackendType, Is.EqualTo(AuthenticationBackendType.Token));
            var tokenAuthenticationInfo = authenticationInfo as TokenAuthenticationInfo;
            Assert.That(tokenAuthenticationInfo.Token, Is.EqualTo(expectedToken));
        }

        [Test]
        public void ShouldGetCertificateAuthenticationInfo()
        {
            // given
            var expectedThumbprint = "123";
            var expectedCertificate = new X509Certificate2();

            vaultConfigurationProviderMock.Setup(x => x.GetAuthenticationMode()).Returns("certificate");
            vaultConfigurationProviderMock.Setup(x => x.GetCertificateThumbprint()).Returns(expectedThumbprint);
            certificateProviderMock.Setup(x => x.GetCertificate(expectedThumbprint)).Returns(expectedCertificate);

            // when
            var authenticationInfo = factory.GetAuthenticationInfo();

            // then
            Assert.That(authenticationInfo.AuthenticationBackendType, Is.EqualTo(AuthenticationBackendType.Certificate));
            var certificateAuthenticationInfo = authenticationInfo as CertificateAuthenticationInfo;
            Assert.That(certificateAuthenticationInfo.ClientCertificate, Is.EqualTo(expectedCertificate));
        }

        [Test]
        public void ShouldThrowErrorForMissingTokenInTokenAuthenticationMode()
        {
            // given
            vaultConfigurationProviderMock.Setup(x => x.GetAuthenticationMode()).Returns("token");
            vaultConfigurationProviderMock.Setup(x => x.GetTokenKey()).Returns(string.Empty);

            // when
            // then
            Assert.Throws<ArgumentException>(() => factory.GetAuthenticationInfo());
        }

        [Test]
        public void ShouldThrowErrorForUnknownAuthenticationMode()
        {
            // given
            vaultConfigurationProviderMock.Setup(x => x.GetAuthenticationMode()).Returns("notsupported");

            // when
            // then
            Assert.Throws<ArgumentException>(() => factory.GetAuthenticationInfo());
        }
    }
}