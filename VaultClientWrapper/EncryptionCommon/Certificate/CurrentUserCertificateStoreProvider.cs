﻿namespace EncryptionCommon.Certificate
{
    using System.Security.Cryptography.X509Certificates;

    public class CurrentUserCertificateStoreProvider : ICertificateStoreProvider
    {
        public X509Store GetX509Store()
        {
            return new X509Store(StoreName.My, StoreLocation.CurrentUser);
        }
    }
}