﻿namespace EncryptionCommon.Certificate
{
    using System.Security.Cryptography.X509Certificates;

    public class LocalMachineCertificateStoreProvider : ICertificateStoreProvider
    {
        public X509Store GetX509Store()
        {
            return new X509Store(StoreLocation.LocalMachine);
        }
    }
}