﻿namespace EncryptionCommon.Certificate
{
    using System.Security.Cryptography.X509Certificates;

    public interface ICertificateStoreProvider
    {
        X509Store GetX509Store();
    }
}