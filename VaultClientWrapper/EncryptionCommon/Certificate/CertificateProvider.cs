﻿namespace EncryptionCommon.Certificate
{
    using System;
    using System.Linq;
    using System.Security.Cryptography.X509Certificates;

    public class CertificateProvider : ICertificateProvider
    {
        private readonly ICertificateStoreProvider certificateStoreProvider;

        public CertificateProvider(ICertificateStoreProvider certificateStoreProvider)
        {
            this.certificateStoreProvider = certificateStoreProvider;
        }

        public X509Certificate2 GetCertificate(string certificateThumbprint)
        {
            X509Certificate2 encryptionCertificate;
            using (var certificateStore = certificateStoreProvider.GetX509Store())
            { 
                certificateStore.Open(OpenFlags.ReadOnly);

                encryptionCertificate = certificateStore.Certificates.Cast<X509Certificate2>()
                        .FirstOrDefault(certificate => string.Equals(certificate.Thumbprint, certificateThumbprint, StringComparison.OrdinalIgnoreCase));
            }

            if (encryptionCertificate == null)
            {
                throw new ArgumentNullException();
            }

            return encryptionCertificate;
        }
    }
}