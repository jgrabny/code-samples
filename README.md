## code-samples

# VaultClientWrapper

C# wrapper for [VaultSharp library](https://github.com/rajanadar/VaultSharp)

# ExpressionBuilder

Kendo UI custom plugin written in JS (ES5), jQuery, Mocha tests

JSON expression builder to support user-friendly creation of calculations to be 
performed with mathematical and business specific components. Generated JSON is 
parsed by the Evalutator (not included in this repo) which further fetches all
needed parameters and returns output value.

Supports drag and drop, file import/export.

Note: Code usage in not included in this repo as well as Kendo and some 
custom libraries.

![Demo](/ExpressionBuilder/expression-builder.gif)

JSON example: 

``` 
{
  "type": "formula",
  "elements": [
    {
      "type": "value",
      "subtype": "selection",
      "parameters": [
        
      ]
    },
    {
      "type": "operator",
      "subtype": "multiply"
    },
    {
      "type": "value",
      "subtype": "number",
      "parameters": [
        10
      ]
    }
  ]
}
```

# MvcWebAppSample

Sample code from a MVC Web Application.

- CQS and Mediator pattern in Controllers
- HtmlHelper extensions using Builder pattern to support creation of reusable view componenets
- Generic models
- Action filters
- Custom model binders
- Custom serializers
- etc...

Note: Repository does not include any referenced projects and a lot of code is 
stripped down from the production-ready implementation.

