define(['jquery', 'kendo.core.min'], (function($) {
    var kendo = window.kendo,
        Class = kendo.Class;

    var componentMenuItemTextTemplate = kendo.template('<span class="component-id" data-component-id="#: componentId #">#: menuName #</span>');

    var ComponentsStore = Class.extend({
        init: function(options) {
            this.options = $.extend({}, this.options, options);
        },

        options: {
            components: [],
            menuStructure: []
        },

        getKeys: function() {
            return this.options.components.map(function(component) {
                return component.node.subtype.toUpperCase();
            });
        },

        getComponentByNode: function(node) {
            var foundComponents = this.options.components.filter(function(component) {
                return component.matchesNode(node);
            });

            return foundComponents[0];
        },

        getComponentById: function(componentId) {
            var foundComponents = this.options.components.filter(function(component) {
                return component.uid === componentId;
            });

            return foundComponents[0];
        },

        getComponentByHintName: function(hintName) {
            var foundComponents = this.options.components.filter(function(component) {
                if (!component.hasOwnProperty('hintName')) {
                    return false;
                }

                return component.hintName.toUpperCase() === hintName.toUpperCase();
            });

            return foundComponents[0];
        },

        getMenuDefinition: function() {
            return this._mapMenuStructure(this.options.menuStructure);
        },

        _mapMenuStructure: function(items) {
            var that = this;
            return items.reduce(function(result, item) {
                if (!item.hasOwnProperty('component')) {
                    if (item.items != null) {
                        item.items = that._mapMenuStructure(item.items);

                        if (item.items.length === 0) {
                            return result;
                        }
                    } else {
                        item.cssClass = 'informational-element';
                        if (item.tooltip) {
                            item.attr = { title: item.tooltip };
                        }
                    }

                    return result.concat([item]);
                }

                var component = that.getComponentById(item.component);
                if (component == null) {
                    return result;
                }

                var mappedItem = that._mapComponentToMenuItem(component);

                return result.concat([mappedItem]);
            }, []);
        },

        _mapComponentToMenuItem: function(component) {
            var templateData = {
                componentId: component.uid,
                menuName: component.getMenuName()
            };

            return {
                text: componentMenuItemTextTemplate(templateData),
                encoded: false
            };
        }
    });

    return ComponentsStore;
}));
