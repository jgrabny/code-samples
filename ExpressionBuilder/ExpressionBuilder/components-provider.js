define([
    'jquery',
    'g-ajax-module',
    'expression-builder/components',
    'expression-builder/components-store',
    'expression-builder/extensions',
    'kendo.core.min'
], (function($, ajax, components, ComponentsStore) {
    var kendo = window.kendo,
        Class = kendo.Class;

    var BaseComponent = components.BaseComponent,
        OperatorComponent = components.OperatorComponent,
        ValueComponent = components.ValueComponent,
        AttributeValueComponent = components.AttributeValueComponent;

    var valueComponents, selectionComponents, bracketsComponent, functionComponents, operatorComponents;
    var basicComponents = [];

    function registerBasicComponent(component) {
        basicComponents.push(component);
        return component.uid;
    }

    function initBasicComponents(labels) {
        valueComponents = [
            registerBasicComponent(new ValueComponent('Number', {
                'type': 'value',
                'subtype': 'number',
                'parameters': []
            }))
        ];

        selectionComponents = [
            registerBasicComponent(new ValueComponent(labels.selection, {
                'type': 'value',
                'subtype': 'selection',
                'parameters': []
            })),
            registerBasicComponent(new ValueComponent(labels.defaultSelection, {
                'type': 'value',
                'subtype': 'defaultSelection',
                'parameters': []
            })),
            registerBasicComponent(new ValueComponent(labels.numberOfChildrenAssigned,
                {
                    'type': 'value',
                    'subtype': 'numberOfChildrenAssigned',
                    'parameters': []
                }))
        ];

        bracketsComponent = new BaseComponent('&#40;&#41;', {
            'type': 'function',
            'subtype': 'brackets',
            'parameters': []
        });
        bracketsComponent.setMenuName('Brackets');

        functionComponents = [
            registerBasicComponent(bracketsComponent),
            registerBasicComponent(new BaseComponent('Ceiling', {
                'type': 'function',
                'subtype': 'ceiling',
                'parameters': []
            })),
            registerBasicComponent(new BaseComponent('Floor', {
                'type': 'function',
                'subtype': 'floor',
                'parameters': []
            })),
            registerBasicComponent(new BaseComponent('Min', {
                'type': 'function',
                'subtype': 'min',
                'parameters': []
            })),
            registerBasicComponent(new BaseComponent('Max', {
                'type': 'function',
                'subtype': 'max',
                'parameters': []
            }))
        ];

        operatorComponents = [
            registerBasicComponent(new OperatorComponent('&#43;', 'Add', {
                'type': 'operator',
                'subtype': 'add'
            })),
            registerBasicComponent(new OperatorComponent('&#8722;', 'Subtract', {
                'type': 'operator',
                'subtype': 'subtract'
            })),
            registerBasicComponent(new OperatorComponent('&#247;', 'Divide', {
                'type': 'operator',
                'subtype': 'divide'
            })),
            registerBasicComponent(new OperatorComponent('&#215;', 'Multiply', {
                'type': 'operator',
                'subtype': 'multiply'
            })),
        ];
    }

    function toMenuComponent(componentId) {
        return {
            component: componentId
        };
    }

    function getBasicMenuStructure() {
        var valueMenuComponents = valueComponents.map(toMenuComponent);

        var selectionMenuComponents = [{
            text: 'Selection',
            items: selectionComponents.map(toMenuComponent)
        }];
        var functionMenuComponents = [{
            text: 'Function',
            items: functionComponents.map(toMenuComponent)
        }];
        var operatorMenuComponents = [{
            text: 'Operator',
            items: operatorComponents.map(toMenuComponent)
        }];

        return valueMenuComponents
            .concat(selectionMenuComponents)
            .concat(functionMenuComponents)
            .concat(operatorMenuComponents);
    }

    var ComponentsProvider = Class.extend({
        init: function(options) {
            this.options = $.extend({}, this.options, options);

            this.options.disabledComponents = this.options.disabledComponents.map(function(item) {
                return item.toUpperCase();
            });

            var disabledComponents = this.options.disabledComponents;
            initBasicComponents(this.options.labels);
            this.filteredBasicComponents = basicComponents.filter(function(component) {
                return disabledComponents.indexOf(component.name.replace(/\s/g, '').toUpperCase()) < 0;
            });

            this.basicMenuStructure = getBasicMenuStructure();
        },

        options: {
            dynamicComponentsUrl: null,
            disabledComponents: []
        },

        get: function() {
            var that = this;
            var deferred = $.Deferred();

            var result = that.filteredBasicComponents;

            function resolveWith(componentsToResolve, menuStructure) {
                var store = new ComponentsStore({
                    components: componentsToResolve,
                    menuStructure: menuStructure
                });

                deferred.resolve(store);
            }

            if (that.options.dynamicComponentsUrl) {
                ajax.get(that.options.dynamicComponentsUrl)
                    .done(function(response) {
                        if (!response.Success) {
                            deferred.reject(response.Message);
                            return;
                        }

                        var controllerResponse = response.Value;
                        var dynamicComponents = controllerResponse.DynamicComponents.map(function(metadata) {
                            var component = new AttributeValueComponent(metadata.Id, metadata.Name, metadata.Value, metadata.MenuName, metadata.Node);
                            if (metadata.hasOwnProperty('HintName')) {
                                component.hintName = metadata.HintName;
                            }

                            if (metadata.hasOwnProperty('Variant')) {
                                component.setVariant(metadata.Variant);
                            }

                            return component;
                        });

                        var newComponents = result.concat(dynamicComponents);
                        var menuStructure = that.basicMenuStructure.concat(controllerResponse.MenuStructure);

                        resolveWith(newComponents, menuStructure);
                    });
            }
            else {
                resolveWith(result, that.basicMenuStructure);
            }

            return deferred.promise();
        }
    });

    return ComponentsProvider;
}));
