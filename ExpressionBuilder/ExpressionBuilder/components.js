define(['kendo.core.min'], (function() {
    var kendo = window.kendo,
        Class = kendo.Class;

    var BaseComponent = Class.extend({
        init: function(name, node) {
            this.uid = kendo.guid();
            this.name = name;
            this.node = node;
            this.menuName = null;
        },

        setMenuName: function(menuName) {
            this.menuName = menuName;
        },

        getMenuName: function() {
            if (this.menuName == null) {
                return this.name;
            } else {
                return this.menuName;
            }
        },

        matchesNode: function(node) {
            return node.type === this.node.type &&
                   node.subtype === this.node.subtype;
        },

        hasHintName: function() {
            return this.hasOwnProperty('hintName');
        }
    });

    var OperatorComponent = BaseComponent.extend({
        init: function(name, menuName, node) {
            BaseComponent.fn.init.call(this, name, node);
            this.setMenuName(menuName);
        }
    });

    var ValueComponent = BaseComponent.extend({
        init: function(name, node) {
            BaseComponent.fn.init.call(this, name, node);
            this.value = '';
        },

        setValue: function(value) {
            this.value = value;
        },

        setVariant: function(variant) {
            this.variant = variant;
        },

        hasVariant: function() {
            return this.hasOwnProperty('variant');
        }
    });

    var AttributeValueComponent = ValueComponent.extend({
        init: function(id, name, value, menuName, node) {
            ValueComponent.fn.init.call(this, name, node);
            this.uid = id;
            this.setValue(value);
            this.setMenuName(menuName);
        },

        matchesNode: function(node) {
            var baseResult = ValueComponent.fn.matchesNode.call(this, node);
            if (!baseResult) {
                return false;
            }

            return this._matchesParameters(node.parameters);
        },

        _matchesParameters: function(parameters) {
            var selfParameters = this.node.parameters;

            if (selfParameters.length !== parameters.length) {
                return false;
            }

            var that = this;
            return selfParameters.reduce(function(result, selfParameter, index) {
                if (!result) {
                    return result;
                }

                var parameter = parameters[index];

                return that._matches(selfParameter, parameter);
            }, true);
        },

        _matches: function(selfParameter, parameter) {
            for (var propertyName in selfParameter) {
                if (selfParameter.hasOwnProperty(propertyName)) {
                    if (!parameter.hasOwnProperty(propertyName)) {
                        return false;
                    }

                    if(selfParameter[propertyName] !== parameter[propertyName]) {
                        return false;
                    }
                }
            }

            return true;
        }
    });

    return {
        BaseComponent: BaseComponent,
        OperatorComponent: OperatorComponent,
        ValueComponent: ValueComponent,
        AttributeValueComponent: AttributeValueComponent
    };
}));
