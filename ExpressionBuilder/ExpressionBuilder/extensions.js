define([], (function() {
    Array.prototype.flatten = function() {
        return this.reduce(function(flat, toFlatten) {
            return flat.concat(Array.isArray(toFlatten) ? toFlatten.flatten() : toFlatten);
        }, []);
    };

    String.prototype.toDisplayString = function() {
        return this.replace(/\w\S*/g, function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    };
}));
