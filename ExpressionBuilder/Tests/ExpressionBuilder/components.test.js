define(['jquery', 'expression-builder/components'], (function($, components) {
    describe('AttributeValueComponent', function() {
        var AttributeValueComponent = components.AttributeValueComponent;

        var node, attributeComponent;

        beforeEach(function() {
            node = {
                type: 'value',
                subtype: 'salary',
                parameters: [{
                    property1: 'value1',
                    property2: 'value2'
                }]
            };

            attributeComponent = new AttributeValueComponent(kendo.guid(), 'test', 'test', 'test', node);
        });

        function createOverridenNode(overridenProperties) {
            return $.extend({}, node, overridenProperties);
        }

        it('should match node with empty parameters', function() {
            // given
            node.parameters = [];
            var matchingNode = createOverridenNode();

            // when
            var result = attributeComponent.matchesNode(matchingNode);

            // then
            expect(result).toBe(true);
        });

        it('should not match node with different subtype', function() {
            // given
            var notMatchingNode = createOverridenNode({ subtype: 'different' });

            // when
            var result = attributeComponent.matchesNode(notMatchingNode);

            // then
            expect(result).toBe(false);
        });

        it('should match node with same parameters', function() {
            // given
            var additionalParameters = {
                parameters: [{
                    property1: 'value1',
                    property2: 'value2'
                }]
            };

            var matchingNode = createOverridenNode(additionalParameters);

            // when
            var result = attributeComponent.matchesNode(matchingNode);

            // then
            expect(result).toBe(true);
        });

        it('should match node with additional property in parameters', function() {
            // given
            var additionalParameters = {
                parameters: [{
                    property1: 'value1',
                    property2: 'value2',
                    additionalProperty: 'value'
                }]
            };

            var matchingNode = createOverridenNode(additionalParameters);

            // when
            var result = attributeComponent.matchesNode(matchingNode);

            // then
            expect(result).toBe(true);
        });

        var testCases = [{
            testName: 'too few parameters',
            parameters: []
        }, {
            testName: 'too much parameters',
            parameters: [1, 2, 3]
        },{
            testName: 'missing property',
            parameters: [{
                property1: 'value1'
            }]
        },{
            testName: 'wrong property value',
            parameters: [{
                property1: 'value1',
                property2: 'wrong value'
            }]
        }];

        testCases.forEach(function(testCase) {
            it('should not match node with different parameters - ' + testCase.testName, function() {
                // given
                var additionalParameters = {
                    parameters: testCase.parameters
                };

                var matchingNode = createOverridenNode(additionalParameters);

                // when
                var result = attributeComponent.matchesNode(matchingNode);

                // then
                expect(result).toBe(false);
            });
        });

        it('should return false if no hintName', function() {
            // when
            var result = attributeComponent.hasHintName();

            // then
            expect(result).toBe(false);
        });

        it('should return true if hintName present', function() {
            // given
            attributeComponent.hintName = 'some hint name';

            // when
            var result = attributeComponent.hasHintName();

            // then
            expect(result).toBe(true);
        });

        it('should return false if no variant', function() {
            // when
            var result = attributeComponent.hasVariant();

            // then
            expect(result).toBe(false);
        });

        it('should return true if variant present', function() {
            // given
            attributeComponent.variant = 'some variant';

            // when
            var result = attributeComponent.hasVariant();

            // then
            expect(result).toBe(true);
        });
    });
}));
