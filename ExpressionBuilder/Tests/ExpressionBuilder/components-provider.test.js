define(['jquery', 'expression-builder/components-provider'], (function($, ComponentsProvider) {
    describe('ComponentsProvider', function() {
        it('should return all basic components when nothing disabled', function(done) {
            var provider = new ComponentsProvider({
                disabledComponents: []
            });

            provider.get()
                .done(function(store) {
                    expect(store.options.components.length).toBe(12);
                })
                .fail(function() {
                    fail('Error getting store');
                })
                .always(function() {
                    done();
                });
        });

        it('should return filtered basic components', function(done) {
            var provider = new ComponentsProvider({
                disabledComponents: ['CurrentSelection']
            });

            provider.get()
                .done(function(store) {
                    var components = store.options.components;
                    expect(components.length).toBe(11);

                    var selectionComponents = components.filter(function(component) {
                        return component.name.toUpperCase() === 'CURRENT SELECTION';
                    });

                    expect(selectionComponents.length).toBe(0);
                })
                .fail(function() {
                    fail('Error getting store');
                })
                .always(function() {
                    done();
                });
        });
    });
}));
