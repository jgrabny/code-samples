define(['jquery', 'expression-builder/components-store'], (function($, ComponentsStore) {
    describe('ComponentsStore', function() {
        var expectedName = 'element name';
        var expectedSubMenuComponentName = 'Submenu component';

        var components, menuStructure, nestedMenuStructure;

        beforeEach(function() {
            components = [{
                uid: kendo.guid(),
                node: {
                    subtype: 'subtype1'
                },
                getMenuName: function() {
                    return expectedName;
                },
                matchesNode: function() {
                    return false;
                }
            },
            {
                uid: kendo.guid(),
                node: {
                    subtype: 'subtype2'
                },
                getMenuName: function() {
                    return expectedSubMenuComponentName;
                },
                matchesNode: function() {
                    return true;
                }
            }];

            menuStructure = [{
                component: components[0].uid
            }];

            var subMenuItem = {
                component: components[1].uid
            };

            var subMenuNode = {
                text: 'Submenu',
                items: [subMenuItem]
            };

            nestedMenuStructure = menuStructure.concat([subMenuNode]);
        });

        function createStore(storeMenuStructure) {
            return new ComponentsStore({
                components: components,
                menuStructure: storeMenuStructure
            });
        }

        function formatMenuText(uid, name) {
            return '<span class="component-id" data-component-id="' + uid + '">' + name + '</span>';
        }

        function expectComponentMenuItem(menuItem, expectedComponent) {
            var expectedText = formatMenuText(expectedComponent.uid, expectedComponent.getMenuName());

            expect(menuItem.text).toBe(expectedText);
            expect(menuItem.encoded).toBe(false);
        }

        it('should map single menu item', function() {
            // given
            var store = createStore(menuStructure);

            // when
            var menuDefinition = store.getMenuDefinition();

            // then
            expect(menuDefinition.length).toBe(1);

            var menuItem = menuDefinition[0];
            expectComponentMenuItem(menuItem, components[0]);
        });

        it('should pass through non-component menu item', function() {
            // given
            var newItem = {
                text: 'New item'
            };

            menuStructure.push(newItem);

            var store = createStore(menuStructure);

            // when
            var menuDefinition = store.getMenuDefinition();

            // then
            expect(menuDefinition.length).toBe(2);

            var menuItem = menuDefinition[1];
            expect(menuItem.text).toBe(newItem.text);
        });

        it('should map submenu', function() {
            // given
            var store = createStore(nestedMenuStructure);

            // when
            var menuDefinition = store.getMenuDefinition();

            // then
            expect(menuDefinition.length).toBe(2);

            var mappedSubMenuNode = menuDefinition[1];
            expect(mappedSubMenuNode.text).toBe('Submenu');
            expect(mappedSubMenuNode.items.length).toBe(1);

            var mappedSubMenuComponent = mappedSubMenuNode.items[0];
            expectComponentMenuItem(mappedSubMenuComponent, components[1]);
        });

        it('should encode menu name', function() {
            // given
            $.extend(components[0], {
                getMenuName: function() {
                    return '<script>alert(1)</script>';
                }
            });
            var store = createStore(menuStructure);

            // when
            var menuDefinition = store.getMenuDefinition();

            // then
            var mappedDangerousNode = menuDefinition[0];
            var expectedText = formatMenuText(components[0].uid, '&lt;script&gt;alert(1)&lt;/script&gt;');
            expect(mappedDangerousNode.text).toBe(expectedText);
        });

        it('should return components keys', function() {
            // given
            var store = createStore(nestedMenuStructure);

            // when
            var keys = store.getKeys();

            // then
            expect(keys.length).toBe(2);
            expect(keys).toContain(components[0].node.subtype.toUpperCase());
            expect(keys).toContain(components[1].node.subtype.toUpperCase());
        });

        it('should return component by node', function() {
            // given
            var store = createStore(menuStructure);

            // when
            var component = store.getComponentByNode({ type: 'test' });

            // then
            expect(component).toBe(components[1]);
        });

        it('should not return any component if no hint name matches', function() {
            // given
            var store = createStore(nestedMenuStructure);

            // when
            var component = store.getComponentByHintName('not existing hint name');

            // then
            expect(component).toBeUndefined();
        });

        it('should return component by case insensitive hint name', function() {
            // given
            var searchedHintName = 'TeStHiNt NaMe';
            $.extend(components[1], {
                hintName: 'testHint name'
            });

            var store = createStore(nestedMenuStructure);

            // when
            var component = store.getComponentByHintName(searchedHintName);

            // then
            expect(component).toBe(components[1]);
        });
    });
}));
