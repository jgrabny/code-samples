// ----------------------------------------------------------
// Helper functions
// ----------------------------------------------------------
function loadJSON(url) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, false);
    xhr.send();
    if (xhr.status === 200) {
        return JSON.parse(xhr.responseText);
    }

    return null;
}

function extend(obj, src) {
    for (var key in src) {
        if (src.hasOwnProperty(key)) {
            obj[key] = src[key];
        }
    }
    return obj;
}

// ----------------------------------------------------------
// Load all files needed to setup RequireJS
// ----------------------------------------------------------
var allTestFiles = [];
var jsonFiles = [];

var TEST_REGEXP = /(spec|test)\.js$/i;
var JSON_REGEXP = /.*RequireJS\.json$/i;

// Get a list of all the test files to include and RequireJS json files to include
Object.keys(window.__karma__.files).forEach(function (file) {
    if (TEST_REGEXP.test(file)) {
        // Normalize paths to RequireJS module names.
        // If you require sub-dependencies of test files to be loaded as-is (requiring file extension)
        // then do not normalize the paths
        var normalizedTestModule = file.replace(/^\/base\/Scripts\/|\.js$/g, '');
        allTestFiles.push(normalizedTestModule);
    }

    if (JSON_REGEXP.test(file)) {
        jsonFiles.push(file);
    }
});

// Read configuration from RequireJS config files and merge it
var paths = {};
var shim = {};

jsonFiles.forEach(function(fileUrl) {
    var json = loadJSON(fileUrl);
    extend(paths, json.paths);
    extend(shim, json.shim);
});

// ----------------------------------------------------------
// Configure RequieJS
// ----------------------------------------------------------
require.config({
    // Karma serves files under /base, which is the basePath from your config file
    baseUrl: '/base/Scripts',

    // dynamically load all test files
    deps: allTestFiles,

    // we have to kickoff jasmine, as it is asynchronous
    callback: window.__karma__.start,

    // setup paths and shim
    paths: paths,
    shim: shim
});
