﻿'use strict';

define('g-expression-builder-widget', [
        'jquery',
        'expression-builder/components-provider',
        'g-ajax-module',
        'g-confirmation-module',
        'g-notification-module',
        'expression-builder/extensions',
        'kendo.data.min',
        'kendo.menu.min',
        'kendo.upload.min',
        'kendo.draganddrop.min',
        'jquery-kendo-helpers'
    ],
    (function($, ComponentsProvider, ajax, confirmation, CustomNotification) {
        var kendo = window.kendo,
            ui = kendo.ui,
            Widget = ui.Widget,
            template = kendo.template,
            Class = kendo.Class,
            ObservableArray = kendo.data.ObservableArray,
            ObservableObject = kendo.data.ObservableObject,
            DataSource = kendo.data.DataSource,

            events = {
                CHANGE: 'change'
            },

            constants = {
                types: {
                    VALUE: 'VALUE',
                    OPERATOR: 'OPERATOR',
                    FUNCTION: 'FUNCTION',
                },
                actions: {
                    REMOVE: 'REMOVE'
                }
            },

            valueDefaultSubtypes = {
                SELECTION: 'SELECTION',
                NUMBER: 'NUMBER'
            },

            defaultSource = {
                'type': 'formula',
                'elements': []
            },

            templates = {
                function: template('<div class="expression-component expression-function" data-uid="#: uid #"><span class="component-name">#= functiontype #</span></div>'), // value is not encoded because it's provided internally as unicode chars
                operator: template('<div class="expression-component expression-operator" data-uid="#: uid #"><span>#= value #</span></div>'), // value is not encoded because it's provided internally as unicode chars
                value: template('<div class="expression-component expression-value #:valueTypeClass# # if(variant) { # expression-value-variant-attribute # } #" data-uid="#: uid #"><span class="component-name" title="#: name #">#: name #</span><span class="value-text" title="#: value #">#: value #</span># if(variant) { # <span class="value-variant">(#: variant #)</span># } #</div>'),
                editValue: template('<div class="expression-component expression-value" data-uid="#: uid #"><span class="component-name">#: name #</span><input class="value-input" type="number" value="#:value#"></div>'),
                expressionError: template('<div class="expression-error"><span>#:errorMessage#</span></div>'),
                insert: '<div class="insert-component"></div>',
                menu: template('<ul class="insert-menu" id="#:id#-insert-menu"></ul>'),
                input: template('<input type="hidden" data-expression name=#:name#>'),
                delete: '<span class="delete-component"></span>',
                editComponentPlaceholder: '<span class="edit-component">&#x2c7;</span>',
                container: '<div class="expression-container"></div>',
                comma: '<div class="expression-comma"><span>&#44;</span></div>',
                button: template('<button class="small btn #:buttonClass#" type="button"><svg class="icon-16 g-icon" focusable="false"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#:iconName#" xlink:xlink="http://www.w3.org/1999/xlink"></use></svg>#:text#</button>'),
                icon: template('<svg class="icon-16 g-icon" focusable="false"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#:iconName#" xlink:xlink="http://www.w3.org/1999/xlink"></use></svg>'),
                invalidComponent: template('<div class="expression-component expression-value expression-value-invalid" data-uid="#: uid #"><span class="component-name">#:name#</span></div>')
            };

        ObservableArray.prototype.indexOfUid = function(uid) {
            return this.map(function(e) {
                return e.uid;
            }).indexOf(uid);
        };

        ObservableObject.prototype.getParent = function() {
            var parent = this.parent();
            if (parent instanceof ObservableArray) {
                return parent.parent();
            }

            return parent;
        };

        ObservableObject.prototype.isFunction = function() {
            return this.type.toUpperCase() === constants.types.FUNCTION;
        };

        ObservableObject.prototype.isOperator = function() {
            return this.type.toUpperCase() === constants.types.OPERATOR;
        };

        ObservableObject.prototype.isValue = function() {
            return this.type.toUpperCase() === constants.types.VALUE;
        };

        ObservableObject.prototype.isParametrizedFunction = function() {
            if (this.isFunction()) {
                if (this.subtype.toUpperCase() !== 'BRACKETS') {
                    return true;
                }
            }

            return false;
        };

        kendo.ui.Upload.fn._supportsDrop = function() {
            return false;
        };

        function deepClone(obj) {
            return JSON.parse(JSON.stringify(obj));
        }

        function reformatNumeric(input) {
            var $text = $(input._text);
            var textValue = $text.val().replace(/,/g, '');
            if ($.isNumeric(textValue)) {
                $text.val(parseFloat(textValue));
            }
        }

        var Validator = Class.extend({
            init: function(options) {
                var that = this;
                that.options = $.extend({}, that.options, options);
            },
            options: {
                errorMessages: {
                    functionCannotBeEmpty: 'A {0} function cannot be empty.',
                    functionRequiresTwoComponents: '{0} function requires exactly two components to be inserted directly into it. To insert a formula, enclose it in brackets.',
                    operatorCannotBeFirstElement: 'Operator component cannot be placed at the beginning of the calculation.',
                    operatorCannotBeInsertedDirectlyIntoFunction: '{0} function does not allow operators to be inserted directly into it. To insert a formula, enclose it in brackets.',
                    operatorCannotBeLastElement: 'Operator component cannot be specified as the last element.',
                    operatorCannotNeighborAnotherOperator: 'Multiple operators cannot be placed next to each other.',
                    operatorRequiredBefore: 'An operator is missing before this component.',
                    componentNotSupportedByFormula: 'Component is not supported by this formula.'
                },
            },
            validate: function(previous, current, next) {
                var that = this;

                that._validateIfAvailable(current);

                switch (current.type.toUpperCase()) {
                    case constants.types.VALUE:
                        {
                            that._validateValue(previous, current, next);
                            break;
                        }
                    case constants.types.OPERATOR:
                        {
                            that._validateOperator(previous, current, next);
                            break;
                        }
                    case constants.types.FUNCTION:
                        {
                            that._validateFunction(previous, current, next);
                            break;
                        }
                    default:
                        {
                            throw new Error('Invalid component type ' + current.type);
                        }
                }
            },
            markAsInvalid: function(node, message) {
                var errors = node.errors || [];
                errors.push(message);
                node.invalid = true;
                node.errors = errors;
            },

            _validateIfAvailable: function(current) {
                var that = this;
                if (that.options.validKeys == null) {
                    return;
                }

                if (that.options.validKeys.indexOf(current.subtype.toUpperCase()) < 0) {
                    this.markAsInvalid(current, this.options.errorMessages.componentNotSupportedByFormula);
                }
            },

            _validateValue: function(previous, current) {
                var that = this;
                that._validateSeparationWithOperator(previous, current);
            },
            _validateFunction: function(previous, current) {
                var that = this;
                that._validateSeparationWithOperator(previous, current);
                that._validateNumberOfCeilingOrFloorParams(current);
                that._validateFunctionContent(current);
            },
            _validateOperator: function(previous, current, next) {
                var that = this,
                    message;
                if (that._isInParametrizedFunction(current)) {
                    that.markAsInvalid(current, kendo.format(that.options.errorMessages.operatorCannotBeInsertedDirectlyIntoFunction, current.getParent().subtype.toDisplayString()));

                    return;
                }

                if (previous == null) { // First element in expression or nested function
                    message = that.options.errorMessages.operatorCannotBeFirstElement;
                    this.markAsInvalid(current, message);
                } else {
                    if (previous.isOperator()) {
                        message = that.options.errorMessages.operatorCannotNeighborAnotherOperator;
                        this.markAsInvalid(current, message);
                    }
                }

                if (next == null) { // Last element in expression or nested function
                    message = that.options.errorMessages.operatorCannotBeLastElement;
                    this.markAsInvalid(current, message);
                }
            },
            _validateFunctionContent: function(current) {
                if (current.parameters.length < 1) {
                    this.markAsInvalid(current, kendo.format(this.options.errorMessages.functionCannotBeEmpty, current.subtype.toDisplayString()));
                }
            },
            _validateSeparationWithOperator: function(previous, current) {
                var that = this;
                if (!that._isInParametrizedFunction(current)) {
                    if (previous != null && !previous.isOperator()) {
                        that.markAsInvalid(current, that.options.errorMessages.operatorRequiredBefore);
                    }
                }
            },
            _validateNumberOfCeilingOrFloorParams: function(current) {
                var that = this;
                if (current.isParametrizedFunction()) {
                    var subtype = current.subtype.toUpperCase();
                    if (subtype === 'FLOOR' || subtype === 'CEILING') {
                        if (current.parameters.length > 0 && current.parameters.length !== 2) {
                            that.markAsInvalid(current, kendo.format(that.options.errorMessages.functionRequiresTwoComponents, subtype.toDisplayString()));
                        }
                    }
                }
            },
            _isInParametrizedFunction: function(node) {
                var parent = node.getParent();
                return (parent.isParametrizedFunction(parent));
            }
        });

        var Renderer = Class.extend({
            init: function(validator, componentsStore, texts) {
                this.validator = validator;
                this.componentsStore = componentsStore;
                this.texts = texts;
            },

            // Renders the 'equals' sign at the beginning of the formula
            renderExpressionStart: function($target, root, options) {
                var that = this;
                var $start = $(templates.operator({
                    value: '&#61', // =
                    uid: kendo.guid()
                }));
                $start.addClass('expression-start');
                $target.append($start);

                if (options.editMode) {
                    $target.append(that._buildInsertHtml(null, root));
                }
            },

            // renders the HTML for given type
            renderType: function($target, previous, current, next, editMode) {
                var that = this,
                    $html;
                that.editMode = editMode;

                var component = this.componentsStore.getComponentByNode(current);

                switch (current.type.toUpperCase()) {
                    case constants.types.VALUE:
                        {
                            $html = that._buildValueHtml(current, component);
                            break;
                        }
                    case constants.types.OPERATOR:
                        {
                            $html = that._buildOperatorHtml(current, component);
                            break;
                        }
                    case constants.types.FUNCTION:
                        {
                            $html = that._buildFunctionHtml(current, component);
                            break;
                        }
                    default:
                        {
                            throw new Error('Invalid component type ' + current.type);
                        }
                }

                if (that.editMode) {
                    that.validator.validate(previous, current, next);
                    var deleteIcon = templates.icon({
                        iconName: '#icon-cross'
                    });
                    var deletePlaceholder = $(templates.delete).append(deleteIcon);

                    $html.append(deletePlaceholder);
                    $html.append(templates.editComponentPlaceholder);
                    $html.data('node', current);
                }

                if (current.invalid) {
                    $html.addClass('node-invalid');
                    $html.attr('title', current.errors.join('\r\n'));
                    delete current['invalid'];
                    delete current['errors'];
                }

                $target.append($html);

                // Render insert HTML at the end of the formula
                if (that.editMode) {
                    var parent = current.getParent();

                    if (parent.isParametrizedFunction() && parent.parameters.indexOfUid(current.uid) + 1 !== parent.parameters.length) {
                        return;
                    }

                    $target.append(that._buildInsertHtml(current, parent));
                }
            },

            _buildInsertHtml: function(data, parent) {
                var $insert = $(templates.insert);

                $insert.data('previousNode', data);
                $insert.data('parentNode', parent);

                return $insert;
            },

            _buildValueHtml: function(data, component) {
                if (component == null) {
                    return this._buildInvalidComponentHtml(data);
                }

                var templateName = 'value';

                var templateData = {
                    uid: data.uid,
                    valueTypeClass: 'expression-value-attribute',
                    name: component.name,
                    value: component.value,
                    variant: component.variant
                };

                if (data.subtype.toUpperCase() === valueDefaultSubtypes.NUMBER) {
                    templateData.valueTypeClass = 'expression-value-number';
                    var value = data.parameters[0];
                    if (value != null) {
                        if (!$.isNumeric(value)) {
                            throw new Error('Invalid value: ' + data.value);
                        }

                        templateData.value = value;
                    }

                    if (this.editMode) {
                        templateName = 'editValue';
                    }
                }

                var $html = $(templates[templateName](templateData));

                return $html;
            },

            _buildFunctionHtml: function(data, component) {
                var that = this;
                var functionName;

                if (component == null) {
                    functionName = this.texts.invalidComponentName;
                    this.validator.markAsInvalid(data, this.texts.invalidComponentMessage);
                } else {
                    functionName = component.name;
                }

                // Create base function html
                var $html = $(templates.function({
                    functiontype: functionName,
                    uid: data.uid
                }));

                // Add insert template as the first element in the function html (if in edit mode)
                if (that.editMode) {
                    $html.prepend(that._buildInsertHtml(null, data));
                }

                // For each child of the function render it's element inside function html
                $(data.parameters).each(function(index, current) {
                    that.renderType($html, data.parameters[index - 1], current, data.parameters[index + 1], that.editMode);
                    if (data.subtype !== 'brackets' && index + 1 !== data.parameters.length) {
                        $html.append($(templates.comma));
                    }
                });

                return $html;
            },

            _buildOperatorHtml: function(data, component) {
                if (component == null) {
                    return this._buildInvalidComponentHtml(data);
                }

                var templateData = {
                    value: component.name,
                    uid: data.uid
                };

                return $(templates.operator(templateData));
            },

            _buildInvalidComponentHtml: function(data) {
                var templateData = {
                    uid: data.uid,
                    name: this.texts.invalidComponentName,
                };

                this.validator.markAsInvalid(data, this.texts.invalidComponentMessage);

                return $(templates.invalidComponent(templateData));
            }
        });

        var ContextMenu = Class.extend({
            init: function(options) {
                this.options = $.extend({}, this.options, options);

                this.source = this.options.source;

                var menuId = '#' + this.options.id + '-insert-menu';
                this.component = $(menuId).kendoContextMenu({
                    filter: 'foo', // I don't want it to be attached to anything, showing/closing is handled manually
                    animation: {
                        close: {
                            duration: 0
                        }
                    },
                    dataSource: this.source,
                    open: this._openHandler,
                    deactivate: $.proxy(this._deactivateHandler, this),
                    closeOnClick: false
                }).data('kendoContextMenu');
            },
            options: {
                source: []
            },
            setDataSource: function(dataSource) {
                this.component.setOptions({
                    dataSource: dataSource
                });
            },
            show: function(target) {
                var that = this;
                that._showInternal($(target));
                that.component.open(target);
            },
            showOnPosition: function(e) {
                var that = this;
                that._showInternal($(e.currentTarget));
                that.component.open(e.pageX, e.pageY);
            },
            bindSelectHandler: function(handler) {
                this.component.bind('select', handler);
            },
            isVisible: function() {
                return this.component.element.is(':visible');
            },
            findSelectedItem: function(selectedItem) {
                var that = this;
                var items = that.source.map($.proxy(that._mapNodeFunction, that));
                var flatItems = items.flatten();

                var result = flatItems.filter(function(item) {
                    return item.text.toUpperCase() === selectedItem.innerText.trim().toUpperCase();
                })[0];

                return result;
            },
            _showInternal: function($target) {
                var that = this;

                if (that.anchor != null) {
                    that.anchor.removeClass('context-menu-opened');
                }

                if (that.component.popup.visible()) {
                    that.component.popup.close(true);
                    that.component.popup.element.kendoStop(true);
                }

                that.anchor = $target;
                that.anchor.addClass('context-menu-opened');
            },
            _openHandler: function(e) {
                if ($(e.item).hasClass('insert-menu')) {
                    $(e.item).find('.k-state-focused').removeClass('k-state-focused');
                }
            },
            _deactivateHandler: function(e) {
                if ($(e.item).hasClass('insert-menu')) {
                    var that = this;
                    that.anchor.removeClass('context-menu-opened');
                }
            },
            _mapNodeFunction: function(item) {
                var that = this;
                if (item.items != null) {
                    return item.items.map(that._mapNodeFunction);
                }

                return item;
            }
        });

        var ExpressionBuilder = Widget.extend({
            init: function(element, options) {
                var that = this;

                Widget.fn.init.call(that, element, options);

                var wrapper = $('<div class=\'g-expression-builder\'></div>');
                var buttonsContainer = $('<div class="expression-buttons"></div>');

                that.element.html(wrapper);
                that.element.prepend(buttonsContainer);
                that.element.wrapper = wrapper;
                that.element.buttonsContainer = buttonsContainer;

                that._setup();
            },

            options: {
                // the name is what it will appear as off the kendo namespace(i.e. kendo.ui.ExpressionBuilder).
                // The jQuery plugin would be jQuery.fn.kendoExpressionBuilder.
                name: 'ExpressionBuilder',
                errorMessages: {
                    default: 'Formula is invalid and cannot be used for calculation.',
                    defaultUpload: 'The calculation contained in this file is not recognised and will not be uploaded.'
                },
                texts: {
                    clear: 'CLEAR',
                    download: 'DOWNLOAD',
                    upload: 'UPLOAD',
                    uploadConfirmationMessage: 'The calculation that you are about to upload will overwrite the existing calculation. Do you want to continue?',
                    uploadConfirmationTitle: 'Upload Calculation',
                    clearConfirmationMessage: 'All components within the calculation will be cleared. Do you want to continue?',
                    clearConfirmationTitle: 'Clear Calculation',
                    invalidComponentName: 'Invalid Component',
                    invalidComponentMessage: 'This component is no longer available and may have been removed or disabled.'
                },
                autoBind: true,
                editMode: false,
                disabled: false,
                formulaName: '',
                inputName: '',
                createFileUrl: '',
                getFileUrl: '',
                loadFileUrl: '',
                dynamicComponentsUrl: ''
            },

            bindData: function(data) {
                var that = this;

                try {
                    var jsonData = data == null ? deepClone(defaultSource) : JSON.parse(data);
                    that.originalSource = jsonData;

                    var source = new ObservableObject(jsonData);
                    that.dataSource = new DataSource({
                        data: source.elements
                    });

                    that._dataSource();
                } catch (e) {
                    if (that.options.editMode) {
                        that.bindData();
                    } else {
                        that._displayError(e);
                    }
                }
            },

            refresh: function() {
                var that = this;
                that._hideNotification();
                try {
                    that._buildHtml(that.dataSource.data());
                    that._bindButtonEvents();
                    if (that.options.editMode) {
                        that._bindEvents();
                        that._setupControls();
                    }
                } catch (e) {
                    that._displayError(e);
                    throw e;
                }
            },

            serialize: function() {
                return JSON.stringify(this._getFormulaObject());
            },

            _setup: function() {
                var that = this;

                var notificationContainer = this.element.closest('.g-form-box').find('.g-notification');
                this.notification = new CustomNotification(notificationContainer);

                var componentsProvider = new ComponentsProvider({
                    disabledComponents: this.options.disabledComponents,
                    dynamicComponentsUrl: this.options.dynamicComponentsUrl,
                    labels: this.options.componentsLabels
                });

                this.element.showProgress();
                componentsProvider.get()
                    .done(function(componentsStore) {
                        that._initialiseBuilder(componentsStore);
                    })
                    .fail(function(error) {
                        that._displayNotification(error);
                    })
                    .always(function() {
                        that.element.hideProgress();
                    });
            },

            _initialiseBuilder: function(componentsStore) {
                this.componentsStore = componentsStore;

                var validator = new Validator({
                    validKeys: componentsStore.getKeys(),
                    errorMessages: this.options.errorMessages
                });

                this.renderer = new Renderer(validator, componentsStore, this.options.texts);

                if (this.options.autoBind) {
                    this.bindData(this.options.data);
                }

                if (this.options.editMode) {
                    this.contextMenu = new ContextMenu({
                        id: this.element[0].id,
                        source: componentsStore.getMenuDefinition()
                    });
                    this.contextMenu.bindSelectHandler($.proxy(this._insertSelectHandler, this));
                    $(this.element.input).prop('disabled', this.options.disabled);
                }
            },

            _insertClickHandler: function(e) {
                var that = this;
                that.contextMenu.show(e.currentTarget);
            },

            _editClickHandler: function(e) {
                var that = this;
                that.contextMenu.showOnPosition(e);
            },

            _deleteClickHandler: function(e) {
                var that = this;
                var selectedNode = $(e.currentTarget).closest('div').data('node');
                var parentNode = selectedNode.getParent();

                that._delete(selectedNode, parentNode);
            },

            _insertSelectHandler: function(e) {
                var selectedComponentId = $(e.item).find('> .k-link .component-id').data('componentId');
                var selectedComponent = this.componentsStore.getComponentById(selectedComponentId);

                if (selectedComponent == null) {
                    return;
                }

                var newNode = selectedComponent.node;
                var $target = this.contextMenu.anchor;
                if ($target.hasClass('edit-component')) {
                    var editedNode = $target.closest('div').data('node');
                    var editedParentNode = editedNode.getParent();

                    this._edit(editedNode, editedParentNode, newNode);
                } else {
                    var previousNode = $target.data('previousNode');
                    var parentNode = $target.data('parentNode');

                    this._insert(newNode, previousNode, parentNode);
                }

                this.contextMenu.component.close();
            },

            _delete: function(nodeToDelete, parentNode) {
                // If parent is the root the children collection is named 'elements' or 'parameters' if it is not the root.
                var childrenCollection = parentNode.hasOwnProperty('parameters') ? 'parameters' : 'elements';

                var index = parentNode[childrenCollection].indexOfUid(nodeToDelete.uid);
                parentNode[childrenCollection].splice(index, 1);
            },

            _insert: function(newNode, previousNode, parentNode) {
                // If parent is the root the children collection is named 'elements' or 'parameters' if it is not the root.
                var childrenCollection = parentNode.hasOwnProperty('parameters') ? 'parameters' : 'elements';

                if (previousNode == null) {
                    // Previous node is null which means that it will be the first child.
                    parentNode[childrenCollection].unshift(newNode);
                } else {
                    // It won't be the first child so we need to locate the index of previous element and insert this next to it.
                    var index = parentNode[childrenCollection].indexOfUid(previousNode.uid);
                    parentNode[childrenCollection].splice(index + 1, 0, newNode);
                }
            },

            _edit: function(editedNode, editedParentNode, newNode) {
                var childrenCollection = editedParentNode.hasOwnProperty('parameters') ? 'parameters' : 'elements';
                var index = editedParentNode[childrenCollection].indexOfUid(editedNode.uid);

                editedParentNode[childrenCollection].splice(index, 1);
                editedParentNode[childrenCollection].splice(index, 0, newNode);
            },

            _valueInputChangeHandler: function(e) {
                var that = this;
                that._validateNumericInput(e);

                var node = e.element.closest('div').data('node');
                node.parameters = [e.value()];
                that._updateInput();
            },

            _getFormulaObject: function() {
                var elements = this.dataSource.data().toJSON();
                var newWrapper = deepClone(this.originalSource);
                newWrapper.elements = deepClone(elements);
                return newWrapper;
            },

            _generateDownloadJson: function() {
                var that = this;

                var formula = this._getFormulaObject();
                formula.elements = formula.elements.map(function(e) {
                    var component = that.componentsStore.getComponentByNode(e);
                    if (component.hasHintName()) {
                        e.hintName = component.hintName;
                    }
                    return e;
                });

                return JSON.stringify(formula);
            },

            _downloadButtonClickHandler: function() {
                var that = this;
                var data = {
                    formulaName: that.options.formulaName,
                    content: that._generateDownloadJson()
                };

                ajax.post(
                    that.options.createFileUrl,
                    $.extend(data, $.getAntiForgeryData()),
                    function() {
                        window.open(that.options.getFileUrl, '_blank');
                    });
            },

            _clearButtonClickHandler: function() {
                var that = this;
                confirmation.ask(that.options.texts.clearConfirmationMessage, that.options.texts.clearConfirmationTitle)
                    .confirmed(function() {
                        that.bindData(null);
                    });
            },

            _dataSource: function() {
                var that = this;

                that.dataSource.read();
                that.dataSource.bind(events.CHANGE, function() {
                    that.refresh();
                });

                that.refresh();
            },

            _buildHtml: function(source) {
                var that = this;
                var $html = $(templates.container);

                that.renderer.renderExpressionStart($html, source.parent(), that.options);

                $(source).each(function(index, current) {
                    that.renderer.renderType($html, source[index - 1], current, source[index + 1], that.options.editMode);
                });

                that.element.buttonsContainer.html('');

                if (that.options.editMode) {
                    $html.append($(templates.menu({
                        id: that.element[0].id
                    })));
                    var $input = $(templates.input({
                        name: that.options.inputName
                    }));
                    that.element.input = $input;
                    that._updateInput();
                    $html.append($input);

                    that.element.buttonsContainer.append($(templates.button({
                        buttonClass: 'clear-button',
                        iconName: '#icon-trash',
                        text: that.options.texts.clear
                    })));
                    that.element.buttonsContainer.append($('<input type="file" name="file" class="upload-button"/>'));
                }

                that.element.buttonsContainer.append($(templates.button({
                    buttonClass: 'download-button',
                    iconName: '#icon-export',
                    text: that.options.texts.download
                })));

                that.element.wrapper.html($html);

                if (that.options.editMode) {
                    $html.addClass('edit');
                }
                that._calculateHeight($html);
            },

            _initializeDragAndDrop: function() {
                var that = this,
                    helpers = {
                        ignoreDropTarget: function($target) {
                            if ($target.hasClass('insert-component')) {
                                $target.addClass('drop-ignore');
                            }
                        },
                        toggleDescendantsVisibility: function($target, visible) {
                            var visibility = visible === true ? 'visible' : 'hidden';
                            $target.find('*').css('visibility', visibility);
                        }
                    };

                that.element.find('.expression-component:not(.expression-start)').addClass('draggable');

                $(that.element).find('.expression-container').each(function(index, item) {
                    var group = kendo.guid();
                    $(item).kendoDraggable({
                        group: group,
                        filter: 'div.draggable',
                        ignore: 'input',
                        autoScroll: true,
                        hint: function(element) {
                            var $hint = $('<div class="expression-hint"></div>');
                            $hint.css({
                                width: element.outerWidth(),
                                height: element.outerHeight()
                            });
                            $hint.append(element.clone().css({
                                'opacity': '0.5',
                                'display': 'block'
                            }).removeAttr('title'));

                            return $hint;
                        },
                        dragstart: function(e) {
                            var closestDivSelector = 'div:first';

                            var prevDiv = e.currentTarget.prevAll(closestDivSelector);
                            var nextDiv = e.currentTarget.nextAll(closestDivSelector);
                            helpers.ignoreDropTarget(prevDiv);
                            helpers.ignoreDropTarget(nextDiv);

                            $(that.element).find('.insert-component:not(.drop-ignore)').addClass('drop-target');

                            that._toggleActiveState(e.currentTarget, true, 'drag-active');
                            helpers.toggleDescendantsVisibility(e.currentTarget, false);
                        },
                        dragend: function(e) {
                            $(that.element).find('.insert-component').each(function() {
                                // eslint-disable-next-line
                                var item = $(this);
                                item.removeClass('drop-target');
                                item.removeClass('drop-ignore');
                            });
                            that._toggleActiveState(e.currentTarget, false, 'drag-active');
                            helpers.toggleDescendantsVisibility(e.currentTarget, true);
                        }
                    });

                    $(item).kendoDropTargetArea({
                        group: group,
                        filter: '.insert-component',
                        drop: function(e) {
                            var targetPreviousNode = e.dropTarget.data('previousNode');
                            var targetParentNode = e.dropTarget.data('parentNode');
                            var draggedNode = e.draggable.currentTarget.data('node');

                            var nodeCopy = deepClone(draggedNode);

                            if (targetPreviousNode != null && draggedNode.uid === targetPreviousNode.uid) {
                                return;
                            }

                            that._delete(draggedNode, draggedNode.getParent());
                            that._insert(nodeCopy, targetPreviousNode, targetParentNode);
                        },
                        dragenter: function(e) {
                            if (!e.dropTarget.hasClass('drop-ignore')) {
                                that._toggleActiveState(e.dropTarget, true, 'drop-active');
                            }
                        },
                        dragleave: function(e) {
                            that._toggleActiveState(e.dropTarget, false, 'drop-active');
                        },
                    });
                });
            },

            _initializeUpload: function() {
                var that = this;

                $(that.element).find('.upload-button').each(function(index, element) {
                    $(element).kendoUpload({
                        async: {
                            saveUrl: that.options.loadFileUrl
                        },
                        success: function(e) {
                            if (that.dataSource.data().length === 0) {
                                that._uploadData(e.response);
                            } else {
                                confirmation.ask(that.options.texts.uploadConfirmationMessage, that.options.texts.uploadConfirmationTitle)
                                    .confirmed(that._uploadData.bind(that, e.response));
                            }

                        },
                        error: function() {
                            that._displayNotification(that.options.errorMessages.defaultUpload);
                        },
                        select: function(e) {
                            if (e.files[0].size > 500000) {
                                that._displayNotification('Maximum allowed file size is 512KB.');
                                e.preventDefault();
                            }
                        },
                        localization: {
                            select: that.options.texts.upload,
                            dropFilesHere: ''
                        },
                        multiple: false,
                        showFileList: false,
                        dropZone: '',
                        validation: {
                            maxFileSize: 500000
                        }
                    });
                    $(element).before($(templates.icon({
                        iconName: '#icon-import'
                    })));
                });
            },

            _uploadData: function(data) {
                var defaultData = this.serialize();
                try {
                    var correctedData = JSON.parse(data);
                    correctedData.elements = correctedData.elements.map(this._replaceUsingHintNameIfNeeded.bind(this));
                    this.bindData(JSON.stringify(correctedData));
                } catch (e) { // eslint-disable-line
                    this.bindData(defaultData);
                    this._displayNotification(this.options.errorMessages.defaultUpload);
                }
            },

            _replaceUsingHintNameIfNeeded: function(element) {
                var hintName;
                if (element.hasOwnProperty('hintName')) {
                    hintName = element.hintName;
                    delete element.hintName;
                }

                var component = this.componentsStore.getComponentByNode(element);
                if (component == null && hintName != null) {
                    component = this.componentsStore.getComponentByHintName(hintName);
                    if (component != null) {
                        return component.node;
                    }
                }

                return element;
            },

            _initializeNumericTextBoxes: function() {
                var that = this;
                $(that.element).find('input.value-input').each(function(index, element) {
                    var input = $(element).kendoNumericTextBox({
                        min: -9999999.99999,
                        max: 9999999.99999,
                        decimals: 5,
                        format: 'n5',
                        spinners: false,
                        change: function() {
                            $.proxy(that._valueInputChangeHandler, that, this)();
                        }
                    }).data('kendoNumericTextBox');
                    reformatNumeric(input);
                    $(input._text).on('blur', function() {
                        reformatNumeric(input);
                    });
                    $(input.element).on('blur', function() {
                        reformatNumeric(input);
                    });

                    that._validateNumericInput(input);
                });
            },

            _setupControls: function() {
                var that = this;

                that._initializeNumericTextBoxes();
                that._initializeUpload();
                that._initializeDragAndDrop();
            },

            _toggleActiveState: function(element, state, activeClass) {
                var activeClassToUse = activeClass == null ? 'active' : activeClass;
                element.toggleClass(activeClassToUse, state);
            },

            _bindEvents: function() {
                var that = this;
                $(that.element).find('.insert-component').on('click', $.proxy(that._insertClickHandler, that));
                $(that.element).find('.delete-component').on('click', $.proxy(that._deleteClickHandler, that));
                $(that.element).find('.edit-component').on('click', $.proxy(that._editClickHandler, that));

                $(that.element).find('.expression-component:not(.expression-start)').on('mouseenter', function() {
                    // eslint-disable-next-line
                    var currentElement = $(this);
                    if (currentElement.find('.active').length === 0) {
                        that._toggleActiveState(currentElement, true);
                    }
                    that._toggleActiveState(currentElement.parent(), false);
                });
                $(that.element).find('.expression-component:not(.expression-start)').on('mouseleave', function() {
                    // eslint-disable-next-line
                    var currentElement = $(this);
                    var currentElementParent = currentElement.parent();
                    if (currentElementParent.hasClass('expression-component')) {
                        that._toggleActiveState(currentElementParent, true);
                    }
                    that._toggleActiveState(currentElement, false);
                });
            },

            _bindButtonEvents: function() {
                var that = this;
                $(that.element).find('.download-button').on('click', $.proxy(that._downloadButtonClickHandler, that));
                $(that.element).find('.clear-button').on('click', $.proxy(that._clearButtonClickHandler, that));
            },

            _validateNumericInput: function(input) {
                var element = input.element;
                if (input.value() == null) {
                    element.closest('span.k-numerictextbox').addClass('g-error');
                } else {
                    element.closest('span.k-numerictextbox').removeClass('g-error');
                }
            },

            _updateInput: function() {
                var that = this;
                if (that.dataSource.data().length > 0) {
                    that.element.input.val(that.serialize());
                } else {
                    that.element.input.val('');
                }
            },

            _displayError: function() {
                var that = this;
                var $html = $(templates.container);

                $html.append(templates.expressionError({
                    errorMessage: that.options.errorMessages.default
                }));

                that.element.wrapper.html($html);
                that.element.wrapper.append(that.element.input);
                that._calculateHeight($html);
            },

            _displayNotification: function(message) {
                var that = this;
                if (that.notification != null) {
                    that._hideNotification();
                    that.notification.show({
                            message: message
                        },
                        'error');
                }
            },

            _hideNotification: function() {
                var that = this;
                if (that.notification != null) {
                    that.notification.hide();
                }
            },

            _calculateHeight: function($container) {
                var maxHeight = 400;
                var height = $container.height() + 60;
                if (height < maxHeight) {
                    if ($container.get(0).scrollWidth > $container.width()) {
                        height += ($container.get(0).parentNode.offsetWidth - $container.get(0).offsetWidth);
                    }
                    $container.parent().height(height);
                } else {
                    $container.parent().height(maxHeight);
                }
            }
        });

        ui.plugin(ExpressionBuilder);
    }));