﻿namespace WebAppMvcSample.Models
{
    public class HierarchyTreeModel<T>
    {
        [JsonProperty("hasChildren")]
        public bool HasChildren { get; set; }

        public T Id { get; set; }

        public T ParentId { get; set; }

        public bool Expanded { get; set; }

        public void SetRootParentId()
        {
            ParentId = default(T);
        }
    }
}