﻿namespace WebAppMvcSample.Models
{
    public class Money
    {
        public Money(decimal value, CurrencyCode currency)
        {
            Value = value;
            Currency = currency;
        }

        public decimal Value { get; }

        public CurrencyCode Currency { get; }

        public string Text => ToString();

        public static Money Zero()
        {
            return new Money(0, CurrencyCode.GBP);
        }

        public override string ToString()
        {
            return Value.ToString(Constants.Money.CurrencyFormat, Currency.ToCultureInfo());
        }

        public string ToString(int precision)
        {
            return Value.ToString($"c{precision}", Currency.ToCultureInfo());
        }
    }
}