﻿namespace WebAppMvcSample.Models
{
    public class CheckBoxItemModel<T>
    {
        public CheckBoxItemModel()
        {
        }

        public CheckBoxItemModel(T id, string name, bool selected)
        {
            Id = id;
            Label = name;
            Title = name;
            Selected = selected;
            Enabled = true;
        }

        public T Id { get; set; }

        public string Label { get; set; }

        public bool Selected { get; set; }

        public bool Enabled { get; set; }

        public string AdditionalInformation { get; set; }

        public string Title { get; set; }
    }
}