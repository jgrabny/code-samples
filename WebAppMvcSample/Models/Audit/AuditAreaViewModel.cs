﻿namespace WebAppMvcSample.Models.Audit
{
    public class AuditAreaViewModel
{
        public string Id { get; set; }

        public string Name { get; set; }
    }
}