﻿namespace WebAppMvcSample.Models.Audit
{
    using System.Collections.Generic;
    using System.Linq;

    public class ErrorActionItemModel : ActionItemModel
    {
        public ErrorActionItemModel(string title)
        {
            Title = title;
        }

        public string Title { get; }

        public string Text { get; set; }

        public IEnumerable<ItemValidationMessage> BusinessErrors { get; set; }

        public ExceptionInformation ExceptionInformation { get; set; }

        public string Payload { get; set; }

        public bool HasBusinessErrors => BusinessErrors != null && BusinessErrors.Any();

        public bool IsEmpty => ExceptionInformation == null &&
                               string.IsNullOrEmpty(Text) &&
                               !HasBusinessErrors;
    }
}