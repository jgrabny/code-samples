﻿namespace WebAppMvcSample.Models.Audit
{
    public class ExceptionData
    {
        public ExceptionData(string message, string stackTrace, string exceptionType)
        {
            Message = message;
            StackTrace = stackTrace;
            ExceptionType = exceptionType;
        }

        public string Message { get; }

        public string StackTrace { get; }

        public string ExceptionType { get; }
    }
}