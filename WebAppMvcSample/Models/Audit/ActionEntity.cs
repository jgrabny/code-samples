﻿namespace WebAppMvcSample.Models.Audit
{
    public class ActionEntity
    {
        public ActionEntity(string name, string newValue, string oldValue, string additional = "false")
        {
            Name = name;
            NewValue = newValue;
            OldValue = oldValue;
            Additional = additional;
        }

        public string Name { get; set; }

        public string NewValue { get; set; }

        public string OldValue { get; set; }

        public string Additional { get; set; }
    }
}