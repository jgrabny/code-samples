﻿namespace WebAppMvcSample.Models.Audit
{
    using System;
    using System.Text;

    public class ActionDuration
    {
        public ActionDuration(TimeSpan duration)
        {
            Duration = duration;
        }

        public TimeSpan Duration { get; set; }

        public override string ToString()
        {
            if (Duration == TimeSpan.Zero)
            {
                return Shared_Audit.NoDuration;
            }

            if (Duration.TotalSeconds < 1)
            {
                return Shared_Audit.Duration_LessThanSecond;
            }

            var minutesAndSecondsDisplay = new StringBuilder();

            if (Duration.TotalMinutes >= 1)
            {
                minutesAndSecondsDisplay.Append(string.Format(Shared_Audit.Duration_Minutes, (int)Duration.TotalMinutes));
            }

            if (Duration.Seconds > 0)
            {
                if (minutesAndSecondsDisplay.Length > 0)
                {
                    minutesAndSecondsDisplay.Append(" ");
                }

                minutesAndSecondsDisplay.Append(string.Format(Shared_Audit.Duration_Seconds, Duration.Seconds));
            }

            return minutesAndSecondsDisplay.ToString();
        }
    }
}