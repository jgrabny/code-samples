﻿namespace WebAppMvcSample.Models.Audit
{
    public class SimpleActionItemModel : EntityChangeActionItemModel
    {
        public SimpleActionItemModel(string entityName, string actionType, string text) : base(entityName, actionType)
        {
            Text = text;
        }

        public string Text { get; set; }
    }
}