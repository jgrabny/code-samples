﻿namespace WebAppMvcSample.Models.Audit
{
    using System.Collections.Generic;
    using System.Linq;

    public class ExceptionInformation : ExceptionData
    {
        public ExceptionInformation(
            string message,
            string stackTrace,
            string exceptionType,
            IEnumerable<ExceptionData> innerExceptions) : base(message, stackTrace, exceptionType)
        {
            InnerExceptions = new List<ExceptionData>(innerExceptions ?? Enumerable.Empty<ExceptionData>());
        }

        public IReadOnlyCollection<ExceptionData> InnerExceptions { get; }
    }
}