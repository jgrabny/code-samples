﻿namespace WebAppMvcSample.Models.Audit
{
    public abstract class EntityChangeActionItemModel : ActionItemModel
    {
        protected EntityChangeActionItemModel()
        {
        }

        protected EntityChangeActionItemModel(string entityName, string actionType)
        {
            EntityName = entityName;
            ActionType = actionType;
        }

        public string EntityName { get; }

        public string ActionType { get; }
    }
}