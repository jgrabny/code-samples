﻿namespace WebAppMvcSample.Models
{
    public enum AuditType
    {
        None = 0,
        EmployerHistory = 1,
        GroupHistory = 2,
        Employee = 3
    }
}