﻿namespace WebAppMvcSample.Models
{
    using System;

    public interface IDropDownItemViewModel
    {
        Guid Id { get; set; }

        string Name { get; set; }

        bool IsSelected { get; set; }
    }
}