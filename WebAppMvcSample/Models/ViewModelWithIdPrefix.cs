﻿namespace WebAppMvcSample.Models
{
    public abstract class ViewModelWithIdPrefix
    {
        public string IdPrefix { get; set; }

        public string ShortIdPrefix { get; set; }

        public string ApplyShortIdPrefix(string id)
        {
            return ApplyPrefix(id, ShortIdPrefix);
        }

        public string ApplyIdPrefix(string id)
        {
            return ApplyPrefix(id, IdPrefix);
        }

        private string ApplyPrefix(string id, string prefix)
        {
            return string.Format(id, !string.IsNullOrWhiteSpace(prefix) ? prefix : string.Empty);
        }
    }
}