﻿namespace WebAppMvcSample.Models
{
    public enum RequestType
    {
        AjaxCall = 0,
        Redirection = 1,
        Manual = 2,
        OpenDialog = 3
    }
}