﻿namespace WebAppMvcSample.Models
{
    public class FormViewModel<TModel> where TModel : ViewModelWithIdPrefix, new()
    {
        public FormViewModel(TModel formModel, string addPrefix, string editPrefix, bool? deferred = null)
        {
            FormModel = formModel;
            Deferred = deferred ?? formModel == null;

            if (formModel == null)
            {
                FormModel = new TModel
                {
                    IdPrefix = addPrefix,
                    ShortIdPrefix = PreAndSuffixConstants.Form.AddShortIdPrefix
                };
            }
            else
            {
                IsEdit = true;
                FormModel.IdPrefix = editPrefix;
                FormModel.ShortIdPrefix = PreAndSuffixConstants.Form.EditShortIdPrefix;
            }
        }

        public FormViewModel(TModel formModel) : this(formModel, PreAndSuffixConstants.Form.AddShortIdPrefix, PreAndSuffixConstants.Form.EditShortIdPrefix)
        {
        }

        public FormViewModel(TModel formModel, string subPrefix) : this(formModel, $"{PreAndSuffixConstants.Form.AddShortIdPrefix}{subPrefix}-", $"{PreAndSuffixConstants.Form.EditShortIdPrefix}{subPrefix}-")
        {
        }

        public FormViewModel() : this(null, string.Empty, string.Empty)
        {
        }

        public TModel FormModel { get; set; }

        public bool Deferred { get; private set; }

        public bool IsEdit { get; private set; }

        public void OverrideDeferred(bool value)
        {
            Deferred = value;
        }
    }
}