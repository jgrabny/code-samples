﻿namespace WebAppMvcSample.Infrastructure.Extensions.Builders.Accordion
{
    using System;
    using System.Linq;

    public class AccordionItem
    {
        public AccordionItem(string label, Func<IHtmlString> contentProvider)
        {
            Label = label;
            ContentProvider = contentProvider;
            PermissionType = PermissionType.Read;
        }

        public string Label { get; private set; }

        public string ContainerId { get; set; }

        public IHtmlString TopContent { get; private set; }

        public Func<IHtmlString> ContentProvider { get; private set; }

        public IHtmlString LabelContent { get; private set; }

        public bool HasLabelContent => LabelContent != null;

        public bool HasTopContent => TopContent != null;

        public bool HasContainer => !string.IsNullOrWhiteSpace(ContainerId);

        public string Description { get; private set; }

        public bool CanBeDisplayed { get; private set; } = true;

        public PermissionType PermissionType { get; private set; }

        public CompareOptions CompareOptions { get; private set; }

        private string[] FeatureKeys { get; set; }

        public AccordionItem SetFeatureKey(string featureKey)
        {
            FeatureKeys = new[] { featureKey };
            return this;
        }

        public AccordionItem SetFeatureKeys(string[] featureKeys, CompareOptions compareOptions = CompareOptions.Or)
        {
            FeatureKeys = featureKeys;
            CompareOptions = compareOptions;
            return this;
        }

        public AccordionItem SetPermission(PermissionType permissionType)
        {
            PermissionType = permissionType;
            return this;
        }

        public AccordionItem WithContainer(string containerId)
        {
            ContainerId = containerId;
            return this;
        }

        public AccordionItem WithTopContent(IHtmlString topContent)
        {
            TopContent = topContent;
            return this;
        }

        public AccordionItem WithLabelContent(IHtmlString labelContent)
        {
            LabelContent = labelContent;
            return this;
        }

        public AccordionItem WithDescription(string description)
        {
            Description = description;
            return this;
        }

        public AccordionItem CanDisplayAccordionItem(bool canBeDisplayed)
        {
            CanBeDisplayed = canBeDisplayed;
            return this;
        }

        public bool CanDisplayAccordionItem(Trimmer trimmer)
        {
            if (FeatureKeys == null || FeatureKeys.Length == 0 || FeatureKeys.All(string.IsNullOrEmpty))
            {
                return trimmer.CanDisplay();
            }

            return CompareOptions == CompareOptions.Or
                ? FeatureKeys.Any(x => trimmer.CanDisplay(x, PermissionType))
                : FeatureKeys.All(x => trimmer.CanDisplay(x, PermissionType));
        }
    }
}