﻿namespace WebAppMvcSample.Infrastructure.Extensions.Builders.Accordion
{
    using System;
    using System.Collections.Generic;

    public class AccordionItemFactory
    {
        private readonly IList<AccordionItem> items;

        public AccordionItemFactory(IList<AccordionItem> items)
        {
            this.items = items;
        }

        public AccordionItem Add(string label, Func<IHtmlString> contentProvider)
        {
            var item = new AccordionItem(label, contentProvider);

            items.Add(item);

            return item;
        }

        public AccordionItem Add(string label, Func<IHtmlString> contentProvider, Func<bool> predicate)
        {
            if (predicate.Invoke())
            {
                return Add(label, contentProvider);
            }

            return new AccordionItem(label, contentProvider);
        }
    }
}