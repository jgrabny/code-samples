﻿namespace WebAppMvcSample.Infrastructure.Extensions.Builders.Accordion
{
    using System;
    using System.Collections.Generic;

    public class Accordion : IHtmlString
    {
        private readonly Trimmer trimmer = Trimmer.Instance;

        private readonly TagBuilder tagBuilder;

        private readonly IList<AccordionItem> items;

        public Accordion()
        {
            items = new List<AccordionItem>();
            tagBuilder = new TagBuilder("ul");
            tagBuilder.AddCssClass("accordion");
        }

        public Accordion Items(Action<AccordionItemFactory> addAction)
        {
            var factory = new AccordionItemFactory(items);

            addAction(factory);

            return this;
        }

        public string ToHtmlString()
        {
            if (items.Count == 0)
            {
                return string.Empty;
            }

            bool itemsRendered = false;
            foreach (AccordionItem item in items)
            {
                itemsRendered |= CreateAccordionItem(item);
            }

            return itemsRendered ? tagBuilder.ToString() : string.Empty;
        }

        private bool CreateAccordionItem(AccordionItem item)
        {
            if (!item.CanBeDisplayed)
            {
                return false;
            }

            if (item.CanDisplayAccordionItem(trimmer) == false)
            {
                return false;
            }

            var id = string.Concat("ac-", item.Label.Replace(" ", string.Empty));
            var li = new TagBuilder("li");
            var input = new TagBuilder("input");

            input.MergeAttributes(new Dictionary<string, string>()
            {
                { "type", "checkbox" },
                { "checked", string.Empty },
                { "id", id }
            });

            var label = CreateAccordionItemLabel(item, id);
            var content = CreateAccordionItemContent(item);

            li.AppendInner(input)
                .AppendInner(label)
                .AppendInner(content);

            tagBuilder.AppendInner(li);
            return true;
        }

        private TagBuilder CreateAccordionItemLabel(AccordionItem item, string targetId)
        {
            var label = new TagBuilder("label").WithAttribute("for", targetId)
                .AppendInner(
                    HtmlTags.TagWithClass("span", "circle")
                        .AppendInner(HtmlTags.Icon("icon-16", "#icon-arrowdown")))
                .AppendInner(new TagBuilder("h2").WithInnerText(item.Label));

            var labelItem = HtmlTags.TagWithClass("header", "accordion-label-item");

            if (item.HasLabelContent)
            {
                var labelContentElement = HtmlTags.TagWithClass("div", "accordion-item")
                    .WithCssClass("accordion-label-content")
                    .AppendInner(item.LabelContent);

                labelItem.AppendInner(labelContentElement);
            }

            labelItem.AppendInner(label);
            if (!string.IsNullOrEmpty(item.Description))
            {
                 labelItem.AppendInner(CreateAccordionDescription(item));
            }

            return labelItem;
        }

        private TagBuilder CreateAccordionDescription(AccordionItem item)
        {
            return new TagBuilder("p").WithInnerText(item.Description);
        }

        private TagBuilder CreateAccordionItemContent(AccordionItem item)
        {
            var content = new TagBuilder("div");
            content.AddCssClass("accordion-item");

            content.AppendInnerIf(
                () => item.HasTopContent,
                () => item.TopContent);

            content.AppendInner(HtmlTags.TagWithClass("div", "box")
                .AppendInnerIf(
                    () => item.HasContainer,
                    () => HtmlTags.RawDiv(item.ContainerId, "g-has-progress").AppendInner(item.ContentProvider.Invoke()),
                    () => item.ContentProvider.Invoke()));

            return content;
        }
    }
}