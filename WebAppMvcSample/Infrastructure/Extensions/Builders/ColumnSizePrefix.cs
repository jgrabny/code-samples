﻿namespace WebAppMvcSample.Infrastructure.Extensions.Builders
{
    public enum ColumnSizePrefix
    {
        XS,
        SM,
        MD,
        LG
    }
}