﻿namespace WebAppMvcSample.Infrastructure.Extensions
{
    using System.Reflection;

    using WebAppMvcSample.Controllers;

    public static class AutofacExtensions
    {
        public static void RegisterCommonHandlersAndEvents(this ContainerBuilder builder)
        {
            builder.RegisterClientHandlersAndDecorators(typeof(BaseController).Assembly);
        }

        public static void RegisterBaseController(
            this ContainerBuilder builder,
            Assembly entryAssembly)
        {
            builder.RegisterControllers(typeof(BaseController).Assembly).PropertiesAutowired();
        }

        public static void RegisterCommonModules(
            this ContainerBuilder builder,
            Assembly[] referencedAssemblies)
        {
            builder.RegisterAssemblyModules(referencedAssemblies);
        }
    }
}