﻿namespace WebAppMvcSample.Infrastructure.Audit
{
    using System;

    [AttributeUsage(AttributeTargets.Class)]
    public sealed class ExceptionPublishingHandlerAttribute : HandleErrorAttribute
    {
        public ILogger Logger { get; set; }

        public IEventUtilityService EventUtilityService { get; set; }

        public IRequestDataResolver WebRequestDataResolver { get; set; }

        public IRequestIdProvider RequestIdProvider { get; set; }

        public override void OnException(ExceptionContext filterContext)
        {
            var exception = filterContext.Exception;
            WebRequestData webRequestData = ExtractWebRequestData();

            if (!ShouldPassThroughTheException(exception))
            {
                PublishErrorEventWithLoggingFallback(exception, webRequestData);
            }

            base.OnException(filterContext);
        }

        private WebRequestData ExtractWebRequestData()
        {
            WebRequestData webRequestData = null;
            object dataObject;

            if (WebRequestDataResolver.TryResolveRequestData(out dataObject))
            {
                webRequestData = dataObject as WebRequestData;
            }

            return webRequestData
                   ?? new WebRequestData { RequestId = RequestIdProvider.RequestId };
        }

        private void PublishErrorEventWithLoggingFallback(
          Exception exception,
          WebRequestData data)
        {
            try
            {
                var payload = new PayloadData
                {
                    RequestIdentifier = data.RequestId,
                    EmployerIdentifier = data.CurrentEmployerId,
                    UserName = data.UserName
                };

                var owner = OwnerWrapper.GetEmployerOwner(data.CurrentEmployerId);

                var errorInformation = ErrorInformationBuilder.Create(payload)
                    .WithException(exception)
                    .Build();

                var errorEvent =
                      EventsBuilder.For(payload)
                          .AppendErrorEvent(owner, errorInformation)
                          .Build()
                          .First();

                EventUtilityService.Publish(errorEvent);
            }
            catch (Exception ex)
            {
                Logger.Log("Unable to publish request error - reason", ex);
                Logger.Log("Unable to publish request error - original exception", exception);
                throw;
            }
        }

        private bool ShouldPassThroughTheException(Exception exception)
        {
            return exception is FaultException<BusinessFault>;
        }
    }
}