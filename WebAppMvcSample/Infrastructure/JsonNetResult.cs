namespace WebAppMvcSample.Infrastructure
{
    using System;
    using System.Text;

    public class JsonNetResult : ActionResult
    {
        private JsonNetResult()
        {
            SerializerSettings = new JsonSerializerSettings();
        }

        public Encoding ContentEncoding { get; set; }

        public string ContentType { get; set; }

        public object Data { get; set; }

        public Formatting Formatting { get; set; }

        public JsonSerializerSettings SerializerSettings { get; set; }

        public static JsonNetResult CreateFrom(object data)
        {
            return new JsonNetResult { Data = data };
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            var response = context.HttpContext.Response;

            response.ContentType = !string.IsNullOrEmpty(ContentType)
                ? ContentType
                : "application/json";

            if (ContentEncoding != null)
            {
                response.ContentEncoding = ContentEncoding;
            }

            if (Data != null)
            {
                var writer = new JsonTextWriter(response.Output) { Formatting = Formatting };

                var serializer = JsonSerializer.Create(SerializerSettings);
                serializer.Serialize(writer, Data);

                writer.Flush();
            }
        }
    }
}