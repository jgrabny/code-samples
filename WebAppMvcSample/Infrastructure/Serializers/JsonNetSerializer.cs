﻿namespace WebAppMvcSample.Infrastructure.Serializers
{
    public class JsonNetSerializer : IJavaScriptSerializer
    {
        private static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            Formatting = Formatting.None,
            DateTimeZoneHandling = DateTimeZoneHandling.Unspecified
        };

        public string Serialize(object value)
        {
            return JsonConvert.SerializeObject(value, Settings);
        }
    }
}