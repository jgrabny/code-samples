﻿namespace WebAppMvcSample.Infrastructure.Serializers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public static class JavaScriptDataSerializer
    {
        public static string SerializeData(IDictionary<string, object> @object)
        {
            var output = new StringBuilder();
            output.Append("{");

            foreach (var keyValuePair in @object)
            {
                var value = keyValuePair.Value;

                output.Append(",")
                    .Append(HttpUtility.JavaScriptStringEncode(keyValuePair.Key, true))
                    .Append(":");

                var @string = value as string;

                if (@string != null)
                {
                    output.Append(HttpUtility.JavaScriptStringEncode(@string, true));

                    continue;
                }

                var dictionary = value as IDictionary<string, object>;

                if (dictionary != null)
                {
                    output.Append(SerializeData(dictionary));

                    continue;
                }

                var nested = value as IEnumerable<IDictionary<string, object>>;

                if (nested != null)
                {
                    AppendArrayOfObjects(output, nested);
                    continue;
                }

                if (value is Guid)
                {
                    output.AppendFormat("\"{0}\"", value);

                    continue;
                }

                output.Append(Serialize(value));
            }

            if (output.Length >= 2)
            {
                output.Remove(1, 1); // Remove the first comma
            }

            output.Append("}");

            return output.ToString();
        }

        private static string Serialize(object value)
        {
            return JsonConvert.SerializeObject(
                value,
                new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });
        }

        private static void AppendArrayOfObjects(StringBuilder output, IEnumerable<IDictionary<string, object>> array)
        {
            output.Append("[");

            if (array.Any())
            {
                foreach (var obj in array)
                {
                    output.Append(SerializeData(obj));
                    output.Append(",");
                }

                output.Remove(output.Length - 1, 1);
            }

            output.Append("]");
        }
    }
}