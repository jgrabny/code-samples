﻿namespace WebAppMvcSample.Infrastructure.Serializers
{
    public class JsonNetSerializerInitializer : JavaScriptInitializer
    {
        public override IJavaScriptSerializer CreateSerializer()
        {
            return new JsonNetSerializer();
        }
    }
}