namespace WebAppMvcSample.Infrastructure.ActionFilters
{
    using System;

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public sealed class AllowOnlyAnonymousFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var context = HttpContextFactory.Current;

            if (context.User == null || context.Request.Url == null)
            {
                return;
            }

            if (context.User.Identity.IsAuthenticated)
            {
                filterContext.Result = new HttpStatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
            }
        }
    }
}