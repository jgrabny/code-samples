﻿namespace WebAppMvcSample.Infrastructure.ModelBinders
{
    using System;
    using System.Globalization;

    public class DateTimeModelBinder : DefaultModelBinder
    {
        private readonly string[] customFormats;

        public DateTimeModelBinder(string[] customFormats)
        {
            this.customFormats = customFormats;
        }

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

            if (value == null || value.AttemptedValue.IsEmpty() || value.AttemptedValue.ToUpperInvariant().Equals("NULL"))
            {
                return null;
            }

            return DateTime.ParseExact(value.AttemptedValue, customFormats, CultureInfo.GetCultureInfo("en-GB"), DateTimeStyles.None);
        }
    }
}