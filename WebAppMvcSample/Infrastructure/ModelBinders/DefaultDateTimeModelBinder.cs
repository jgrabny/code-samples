﻿namespace WebAppMvcSample.Infrastructure.ModelBinders
{
    public class DefaultDateTimeModelBinder : DateTimeModelBinder
    {
        public DefaultDateTimeModelBinder() : base(Constants.Dates.DateTimeModelBinderFormats)
        {
        }
    }
}