﻿namespace WebAppMvcSample.Domain.Queries.Audit
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using WebAppMvcSample.Domain.Services.Events;
    using WebAppMvcSample.Models;
    using WebAppMvcSample.Models.Audit;

    public class GetEmployerActionEventsQueryHandler : IWrappingQueryHandler<GetEmployerActionEventsQuery, IEnumerable<ActionItemModel>>
    {
        private readonly IEmployerAudit employerAuditService;

        private readonly IPayloadRetrieveStrategy payloadDataRetrieveStrategy;

        private readonly AuditEventsTranslationService auditEventsToViewModelService;

        public GetEmployerActionEventsQueryHandler(IEmployerAudit employerAuditService, AuditEventsTranslationService auditEventsToViewModelService, IPayloadRetrieveStrategy payloadDataRetrieveStrategy)
        {
            this.employerAuditService = employerAuditService;
            this.auditEventsToViewModelService = auditEventsToViewModelService;
            this.payloadDataRetrieveStrategy = payloadDataRetrieveStrategy;
        }

        public async Task<ResponseWrapper<IEnumerable<ActionItemModel>>> HandleAsync(GetEmployerActionEventsQuery request)
        {
            var payloadData = payloadDataRetrieveStrategy.RetrievePayload();

            var actionEvents = await employerAuditService.GetActionEvents(request.RequestId, request.ActionId);
            var viewModel = auditEventsToViewModelService.TranslateEventsIntoViewModel(
                actionEvents,
                request.TimezoneOffset,
                payloadData.GroupPortal ? AuditType.GroupHistory : AuditType.EmployerHistory);

            return ResponseWrapper<IEnumerable<ActionItemModel>>.Success(viewModel);
        }
    }
}