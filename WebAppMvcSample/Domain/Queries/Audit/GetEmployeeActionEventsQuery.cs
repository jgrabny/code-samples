﻿namespace WebAppMvcSample.Domain.Queries.Audit
{
    using System;
    using System.Collections.Generic;

    using WebAppMvcSample.Models.Audit;

    public class GetEmployeeActionEventsQuery : IQuery<IEnumerable<ActionItemModel>>
    {
        public GetEmployeeActionEventsQuery(Guid employeeId, Guid requestId, Guid actionId, TimeSpan timezoneOffset)
        {
            EmployeeId = employeeId;
            RequestId = requestId;
            ActionId = actionId;
            TimezoneOffset = timezoneOffset;
        }

        public Guid EmployeeId { get; private set; }

        public Guid RequestId { get; private set; }

        public Guid ActionId { get; private set; }

        public TimeSpan TimezoneOffset { get; private set; }
    }
}