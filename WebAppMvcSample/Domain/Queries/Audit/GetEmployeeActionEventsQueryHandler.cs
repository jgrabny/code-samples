﻿namespace WebAppMvcSample.Domain.Queries.Audit
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using WebAppMvcSample.Domain.Services.Events;
    using WebAppMvcSample.Models;
    using WebAppMvcSample.Models.Audit;

    public class GetEmployeeActionEventsQueryHandler : IWrappingQueryHandler<GetEmployeeActionEventsQuery, IEnumerable<ActionItemModel>>
    {
        private readonly IEmployeeAudit employeeAuditService;

        private readonly AuditEventsTranslationService auditEventsToViewModelService;

        public GetEmployeeActionEventsQueryHandler(IEmployeeAudit employeeAuditService, AuditEventsTranslationService auditEventsToViewModelService)
        {
            this.employeeAuditService = employeeAuditService;
            this.auditEventsToViewModelService = auditEventsToViewModelService;
        }

        public async Task<ResponseWrapper<IEnumerable<ActionItemModel>>> HandleAsync(GetEmployeeActionEventsQuery request)
        {
            var actionEvents = await employeeAuditService.GetActionEvents(request.EmployeeId, request.RequestId, request.ActionId);
            var viewModel = auditEventsToViewModelService.TranslateEventsIntoViewModel(actionEvents, request.TimezoneOffset, AuditType.Employee);

            return ResponseWrapper<IEnumerable<ActionItemModel>>.Success(viewModel);
        }
    }
}