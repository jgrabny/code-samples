﻿namespace WebAppMvcSample.Domain.Queries.Audit
{
    using System;
    using System.Collections.Generic;

    using WebAppMvcSample.Models.Audit;

    public class GetEmployerActionEventsQuery : IQuery<IEnumerable<ActionItemModel>>
    {
        public GetEmployerActionEventsQuery(Guid requestId, Guid actionId, TimeSpan timezoneOffset)
        {
            RequestId = requestId;
            ActionId = actionId;
            TimezoneOffset = timezoneOffset;
        }

        public Guid RequestId { get; private set; }

        public Guid ActionId { get; private set; }

        public TimeSpan TimezoneOffset { get; private set; }
    }
}