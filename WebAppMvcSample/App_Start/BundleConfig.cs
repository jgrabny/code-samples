﻿namespace WebAppMvcSample
{
    public static class BundleConfig
    {
        public static void RegisterCommonBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            BundleTable.EnableOptimizations = true;

            bundles.IgnoreList.Clear();
        }
    }
}
