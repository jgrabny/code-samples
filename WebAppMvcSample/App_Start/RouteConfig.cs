﻿namespace WebAppMvcSample
{
    public static class RouteConfig
    {
        private const string ControllersNamespace = "WebAppMvcSample.Controllers";

        public static void RegisterCommonRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.IgnoreRoute("{tenant}/signalr/{*pathInfo}");

            routes.MapRoute(
                name: "GenericTenantRoute",
                url: "{tenant}/{*path}",
                defaults: new { controller = "Error", action = "NotFound" },
                namespaces: new[] { ControllersNamespace });
        }
    }
}