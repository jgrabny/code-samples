﻿namespace WebAppMvcSample
{
    using System;

    using WebAppMvcSample.Infrastructure.ModelBinders;

    public static class ModelBindersConfig
    {
        public static void RegisterCommonModelBinders()
        {
            ModelBinders.Binders.DefaultBinder = new TrimmedStringModelBinder();

            var dateTimeBinder = new DefaultDateTimeModelBinder();
            ModelBinders.Binders.Add(typeof(DateTime), dateTimeBinder);
            ModelBinders.Binders.Add(typeof(DateTime?), dateTimeBinder);
        }
    }
}