﻿namespace WebAppMvcSample.Controllers
{
    using System;

    [DisableClientCache]
    [ExceptionPublishingHandler]
    [RequirePortalEnabled]
    public class BaseController : AsyncController
    {
        public IDateTimeProvider DateTimeProvider { get; set; }
        public IWrappingRequestMediator Mediator { get; set; }

        protected static TimeSpan GetTimeZoneOffset()
        {
            return HttpContextFactory.Current.GetTimeZoneOffset();
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ViewBag.Today = DateTimeProvider.Today();
            ViewBag.Now = DateTimeProvider.Now();
            base.OnActionExecuting(filterContext);
        }
    }
}