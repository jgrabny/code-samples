﻿namespace WebAppMvcSample.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using WebAppMvcSample.Domain.Queries.Audit;
    using WebAppMvcSample.Models.Audit;

    [Auth]
    [PasswordExpiry]
    public class AuditController : BaseController
    {
        public async Task<ActionResult> GetEmployeeActionEvents(Guid ownerId, Guid requestId, Guid actionId, int timezoneOffset)
        {
            var model =
            (await
                Mediator.HandleAsync<IEnumerable<ActionItemModel>>(
                    new GetEmployeeActionEventsQuery(ownerId, requestId, actionId, TimeSpan.FromMinutes(timezoneOffset)))).UnWrap();

            return PartialView("Audit/_ActionEvents", model);
        }

        public async Task<ActionResult> GetEmployerActionEvents(Guid ownerId, Guid requestId, Guid actionId, int timezoneOffset)
        {
            var model =
            (await
                Mediator.HandleAsync<IEnumerable<ActionItemModel>>(
                    new GetEmployerActionEventsQuery(requestId, actionId, TimeSpan.FromMinutes(timezoneOffset)))).UnWrap();

            return PartialView("Audit/_ActionEvents", model);
        }
    }
}