﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebAppMvcSample.Views.Shared.Audit
{
    using WebAppMvcSample.Infrastructure.Extensions.Builders;
    using WebAppMvcSample.Infrastructure.Extensions.Builders.Button;
    using WebAppMvcSample.Models.Audit;
#line 1 "..\..\Views\Shared\Audit\_AuditFilterPanel.cshtml"
#line default
    #line hidden
    
    #line 2 "..\..\Views\Shared\Audit\_AuditFilterPanel.cshtml"
#line default
    #line hidden
    
    #line 3 "..\..\Views\Shared\Audit\_AuditFilterPanel.cshtml"

#line default
    #line hidden
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Shared/Audit/_AuditFilterPanel.cshtml")]
    public partial class _Views_Shared_Audit__AuditFilterPanel_cshtml : System.Web.Mvc.WebViewPage<ActionGroupListModel>
    {
        public _Views_Shared_Audit__AuditFilterPanel_cshtml()
        {
        }
        public override void Execute()
        {
WriteLiteral("<div");

WriteLiteral(" id=\"audit-date-pickers\"");

WriteLiteral(">\r\n");

            
            #line 7 "..\..\Views\Shared\Audit\_AuditFilterPanel.cshtml"
    
            
            #line default
            #line hidden
            
            #line 7 "..\..\Views\Shared\Audit\_AuditFilterPanel.cshtml"
     using (Html.BeginForm())
    {
        
            
            #line default
            #line hidden
            
            #line 9 "..\..\Views\Shared\Audit\_AuditFilterPanel.cshtml"
   Write(Html.HiddenFor(x => x.TimeZoneOffset, new { Id = "timezone-offset", Name = "ActionGroupListModel.TimeZoneOffset" }));

            
            #line default
            #line hidden
            
            #line 9 "..\..\Views\Shared\Audit\_AuditFilterPanel.cshtml"
                                                                                                                            

        
            
            #line default
            #line hidden
            
            #line 11 "..\..\Views\Shared\Audit\_AuditFilterPanel.cshtml"
   Write(Html.InputFor(x => x.PageIndex).Hidden().WithValue(Model.PageIndex));

            
            #line default
            #line hidden
            
            #line 11 "..\..\Views\Shared\Audit\_AuditFilterPanel.cshtml"
                                                                            
        
            
            #line default
            #line hidden
            
            #line 12 "..\..\Views\Shared\Audit\_AuditFilterPanel.cshtml"
   Write(Html.InputFor(x => x.FirstRenderTime).Hidden().WithValue(Model.FirstRenderTime));

            
            #line default
            #line hidden
            
            #line 12 "..\..\Views\Shared\Audit\_AuditFilterPanel.cshtml"
                                                                                        


            
            #line default
            #line hidden
WriteLiteral("        <div");

WriteLiteral(" class=\"filter-container g-form-box-body\"");

WriteLiteral(">\r\n");

WriteLiteral("            ");

            
            #line 15 "..\..\Views\Shared\Audit\_AuditFilterPanel.cshtml"
        Write(Html.DatePickerFor(x => x.LocalTimeFilterFrom)
                .Required()
                .WithColumnClass(ColumnSizePrefix.MD, Model.WithFilteringByAreas ? ColumnSize.Col3 : ColumnSize.Col5)
                .WithAdditionalClass("audit-date-filter-field")
                .WithInitialValue(Model.LocalTimeFilterFrom)
                .BeforeOrEqualInThePast(ViewBag.Today, Model.LocalTimeFilterTo, Shared_Audit.FilterFromDateBeforeFilterToValidationMessage)
                .Deferred());

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n");

WriteLiteral("            ");

            
            #line 23 "..\..\Views\Shared\Audit\_AuditFilterPanel.cshtml"
        Write(Html.DatePickerFor(x => x.LocalTimeFilterTo)
                .Required()
                .WithColumnClass(ColumnSizePrefix.MD, Model.WithFilteringByAreas ? ColumnSize.Col3 : ColumnSize.Col5)
                .WithAdditionalClass("audit-date-filter-field")
                .WithInitialValue(Model.LocalTimeFilterTo)
                .LaterThanOrEqual(x => x.LocalTimeFilterFrom, Shared_Audit.FilterToDateAfterFilterFromValidationMessage)
                .InThePast(ViewBag.Today)
                .Deferred());

            
            #line default
            #line hidden
WriteLiteral("\r\n        \r\n");

            
            #line 32 "..\..\Views\Shared\Audit\_AuditFilterPanel.cshtml"
            
            
            #line default
            #line hidden
            
            #line 32 "..\..\Views\Shared\Audit\_AuditFilterPanel.cshtml"
             if (Model.WithFilteringByAreas)
            {
                
            
            #line default
            #line hidden
            
            #line 34 "..\..\Views\Shared\Audit\_AuditFilterPanel.cshtml"
            Write(Html.MultiSelectFor(x => x.AuditAreasToFilterBy, Model.AvailableAreas)
                    .WithColumnClass(ColumnSizePrefix.MD, ColumnSize.Col4)
                    .WithKeyProperty("Id")
                    .WithDisplayProperty("Name")
                    .WithInitialText(Shared_Audit.SelectAreas)
                    .Deferred());

            
            #line default
            #line hidden
            
            #line 39 "..\..\Views\Shared\Audit\_AuditFilterPanel.cshtml"
                                
            }

            
            #line default
            #line hidden
WriteLiteral("            <div");

WriteLiteral(" class=\"set-button-container col-md-2\"");

WriteLiteral(">\r\n");

WriteLiteral("                ");

            
            #line 42 "..\..\Views\Shared\Audit\_AuditFilterPanel.cshtml"
           Write(Html.Button().Custom().WithValue(Shared_General.Set).WithId("filter-audit-button").WithType(ButtonType.Submit));

            
            #line default
            #line hidden
WriteLiteral("\r\n            </div>\r\n        </div>\r\n");

            
            #line 45 "..\..\Views\Shared\Audit\_AuditFilterPanel.cshtml"
    }

            
            #line default
            #line hidden
WriteLiteral("</div>");

        }
    }
}
#pragma warning restore 1591
